<?php
/**
* The base configuration for WordPress
*
* The wp-config.php creation script uses this file during the
* installation. You don't have to use the web site, you can
* copy this file to "wp-config.php" and fill in the values.
*
* This file contains the following configurations:
*
* * MySQL settings
* * Secret keys
* * Database table prefix
* * ABSPATH
*
* @link https://codex.wordpress.org/Editing_wp-config.php
*
* @package WordPress
*/
// ** MySQL settings ** //
/** The name of the database for WordPress */

/** MySQL データベースのユーザー名 */
define( 'DB_NAME', 'sample' );

if ($_SERVER['HTTP_HOST'] == 'jun-salon.mydevelop.work') { // develop
    /** MySQL database username */
    define('DB_USER', 'root');
    /** MySQL database password */
    define('DB_PASSWORD', 'tK6qUNuj_');
    /** MySQL hostname */
    define('DB_HOST', 'localhost');
} else {                                                    // local
    /** MySQL database username */
    define( 'DB_USER', 'root' );
    /** MySQL database password */
    define( 'DB_PASSWORD', '' );
    /** MySQL hostname */
    define( 'DB_HOST', '127.0.0.1' );
}

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );
/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
/**
* Authentication Unique Keys and Salts.
*
* Change these to different unique phrases!
* You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
* You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*
* @since 2.6.0
*/
define( 'AUTH_KEY',          'R]_*IP1![gz0&gl:e?2MzxuW0>;afwUJv:xs8eWQ/v{K:rLDFeF*fwv:,&A(..YA' );
define( 'SECURE_AUTH_KEY',   '7a8hs.wd!!@EVLagG;($E-h*aGT/tD[Un0&OH!SxTLHX(e.ME,m/h4CKk}5Tph;{' );
define( 'LOGGED_IN_KEY',     'gL@Z=WZ&[`qGV8/%K%2aH43@^<E.6AvNvThTk4Aj*sF#w,-dUkt-mf9Vj15Qrfg5' );
define( 'NONCE_KEY',         'z[r4I>J$i<E}_;HNvGBGCdl5hl=09/b%[s3b!eq%@1fxEf-arNE9QTs({|c(~NaP' );
define( 'AUTH_SALT',         'E/hPHg#<rZ`!Kp?^cV=D`2?G1MsUOCl2,FvN)~H2QU94St`!!K|AJ[p9zg;QgN<^' );
define( 'SECURE_AUTH_SALT',  '~`X!xIW7ACi-.!%e2a*KF_*,ITa`Un,b~}]]4i1ttrMiwe+!6WF@VTrU_9i6n]O[' );
define( 'LOGGED_IN_SALT',    '-i(XBUt#LCBK 4AY&Xt><AJc8piF-jTAi(Brn`93ypV2ycMFnt;bsW^>KpvcyDv5' );
define( 'NONCE_SALT',        'h!tv+E*Bz4R99W6=5Ok7cU/V_EiT4Z/H3~~1]ch}tB6Z@N@4@(Jt7<5a$nn4zNtr' );
define( 'WP_CACHE_KEY_SALT', 'dt *c)0Tv(vp&j+)}Pwe5(X4sxMFS`84!J:<H1ogRGd ,Y2Bw7-?%$p*&=utD~<+' );
/**
* WordPress Database Table prefix.
*
* You can have multiple installations in one database if you give each
* a unique prefix. Only numbers, letters, and underscores please!
*/
$table_prefix = 'junsalon_';
/* That's all, stop editing! Happy blogging. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
define( 'ABSPATH', dirname( __FILE__ ) . '/' );
/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
