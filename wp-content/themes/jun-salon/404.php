<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */
?>

<?php get_template_part('template-parts/common/head'); ?>
<?php get_header(); ?>
    <div class="page-error">
        <div class="page-error__title">Error: 404 Not Found.</div>
        <p class="page-error__text">ページが削除された、URLが間違っている等の理由によりページが見つかりませんでした。</p>
        <p class="page-error__text"><a href="<?php echo home_url(); ?>">サイトトップに戻る</a></p>
    </div>
<?php get_template_part('template-parts/common/sp_nav'); ?>
<?php get_footer(); ?>
<?php get_template_part('template-parts/common/script'); ?>
