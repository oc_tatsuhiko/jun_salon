<?php
/**
 * The template for displaying attachment page
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */
wp_redirect( home_url('/404.php') );
exit();
