<?php
/**
 * The template for displaying all category
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */

get_template_part('template-parts/common/head');
get_header(); ?>
<?php if (preg_match('/news/', $_SERVER['REQUEST_URI'])): ?>
    <?php get_template_part('template-parts/news/index'); ?>
<?php elseif (preg_match('/style/', $_SERVER['REQUEST_URI'])): ?>
    <?php get_template_part('template-parts/style/index'); ?>
<?php endif; ?>
<?php get_template_part('template-parts/common/sp_nav'); ?>
<?php get_template_part('template-parts/common/sns'); ?>
<?php get_footer(); ?>
<?php get_template_part('template-parts/common/script'); ?>
