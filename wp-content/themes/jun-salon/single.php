<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */

get_template_part('template-parts/common/head');
get_header(); ?>

<main class="main">
    <div class="container-single">
        <?php breadcrumb(); ?>
        <div class="single">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <h1 class="single__title"><?php the_title(); ?></h1>
                    <div class="single__date">Published <?php the_time("Y.m.d"); ?></div>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</main>

<?php get_template_part('template-parts/common/sp_nav'); ?>
<?php get_template_part('template-parts/common/sns'); ?>
<?php get_footer(); ?>
<?php get_template_part('template-parts/common/script'); ?>
