<?php
/**
 * The template for displaying author page
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */
  wp_redirect(home_url());
  exit();
