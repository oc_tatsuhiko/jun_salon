<?php
/**
 * The template for displaying all pages
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */
?>
<?php get_template_part('template-parts/common/head'); ?>
<?php get_header(); ?>
<?php if($_SERVER['REQUEST_URI'] === '/gallery'): ?>
    <?php get_template_part('template-parts/gallery/index'); ?>
<?php //elseif($_SERVER['REQUEST_URI'] === '/contact' || $_SERVER['REQUEST_URI'] === '/privacy-policy' || $_SERVER['REQUEST_URI'] === '/thanks'): ?>
<?php else: ?>
    <?php get_template_part('template-parts/page/index'); ?>
<?php endif; ?>
<?php get_template_part('template-parts/common/sp_nav'); ?>
<?php get_template_part('template-parts/common/sns'); ?>
<?php get_footer(); ?>
<?php get_template_part('template-parts/common/script'); ?>
