<?php
/* パーマリンク設定
 * /%category%/%postname%
 * URLから/categoryを削除
 */
add_filter('user_trailingslashit', 'remcat_function');
function remcat_function($link) {
    return str_replace("/category/", "/", $link);
}
add_action('init', 'remcat_flush_rules');
function remcat_flush_rules() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
add_filter('generate_rewrite_rules', 'remcat_rewrite');
function remcat_rewrite($wp_rewrite) {
    $new_rules = array('(.+)/page/(.+)/?' => 'index.php?category_name='.$wp_rewrite->preg_index(1).'&paged='.$wp_rewrite->preg_index(2));
    $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
}
/*
 * 画像パスの設定
 * 投稿、固定ページ投稿した際に、画像パス'/'を変換
 */
add_action('the_content', 'replaceImagePath');
function replaceImagePath($arg) {
    $content = str_replace('"/assets/images/', '"' . get_bloginfo('template_directory') . '/assets/images/', $arg);
    return $content;
}
/** Seculity **/
// wordpress versionを非表示にする
foreach ( array( 'rss2_head', 'commentsrss2_head', 'rss_head', 'rdf_header', 'atom_head', 'comments_atom_head', 'opml_head', 'app_head' ) as $action ) {
    remove_action( $action, 'the_generator' );
}
foreach (array('wp_generator', 'wlwmanifest_link', 'rsd_link') as $function) {
    remove_action('wp_head', $function);
}
/* 不要CSS　読み込み削除 */
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');
remove_filter('the_content_feed', 'wp_staticize_emoji');
remove_filter('comment_text_rss', 'wp_staticize_emoji');
remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/* サムネイル設定 */
add_theme_support( 'post-thumbnails' );
/* アイキャッチ画像の表示の仕方を指定（切り抜き） */
set_post_thumbnail_size(190,142, true );

/** パンくずリスト　表示 **/
function breadcrumb(){
    global $post;
    $str ='';
    if(!is_admin()){
        $str.= '<ul class="breadcrumb"><li class="breadcrumb__item" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">';
        $str.= '<a class="breadcrumb__link" itemprop="url" href="'. home_url() .'" ><span itemprop="title">ホーム</span></a></li>';
        if(is_category()) {
            $cat = get_queried_object();
            if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<li class="breadcrumb__item" itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a class="breadcrumb__link"  itemprop="url" href="'. get_category_link($ancestor) .'" itemprop="url"><span itemprop="title"><span itemprop="title">'. get_cat_name($ancestor) .'</span></span></a></li>';
                }
            }
            $str.='<li class="breadcrumb__item" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">'. $cat-> cat_name . '</li>';
        } elseif(is_page()){
            if($post -> post_parent != 0 ){
                $ancestors = array_reverse(get_post_ancestors( $post->ID ));
                foreach($ancestors as $ancestor){
                    $str.='<li class="breadcrumb__item" itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a class="breadcrumb__link" itemprop="url" href="'. get_permalink($ancestor).'"><span itemprop="title">'. get_the_title($ancestor) .'</span></a></li>';
                }
            }
            $str.='<li class="breadcrumb__item" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">'. get_the_title() .'</li>';
        } elseif(is_single()){
            $categories = get_the_category($post->ID);
            $cat = $categories[0];
            if($cat -> category_parent != 0){
                $list  = get_category_parents($cat, true);
                $cats  = explode('</a>/', $list);
                foreach($cats as $val){
                    if($val != ''){
                        $str.='<li class="breadcrumb__item" itemscope itemtype="https://data-vocabulary.org/Breadcrumb"> '.$val.'</a></li>';
                    }
                }
            }else{
                $str.='<li class="breadcrumb__item" itemscope itemtype="https://data-vocabulary.org/Breadcrumb"><a class="breadcrumb__link" itemprop="url" href="'. site_url().'/'.$cat->slug.'"><span itemprop="title">'. $cat->cat_name .'</span></a></li>';
            }
            $str.='<li class="breadcrumb__item" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">'. get_the_title($post->ID) . '</li>';
        } else{
            $str.='<li class="breadcrumb__item" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">'. wp_title('', false) .'</li>';
        }
        $str.='</ul>';
    }
    wp_reset_query();
    echo $str;
}
/*
    管理画面に独自ファイルを読み込む
    theme直下にwp-adminディレクトリを設置しそれぞれのファイルを追加
 */
function _register_custom_files() {
    $_current_theme_dir = get_template_directory_uri();
    $_custom_files = '';
    $_custom_files = _add_custom_js($_custom_files,'add_custom.js');// jsを追加
    echo $_custom_files;
}
function _add_custom_js($_custom_files, $_file_name){
    $_current_theme_dir = get_template_directory_uri();
    $_custom_files .= '<script type="text/javascript" src="'
        .$_current_theme_dir
        .'/wp-admin/'
        .$_file_name
        .'"></script>';
    return $_custom_files."\n";
}
function _add_custom_css($_custom_files, $_file_name){
    $_current_theme_dir = get_template_directory_uri();
    $_custom_files .= '<link rel="stylesheet"  type="text/css" href="'
        .$_current_theme_dir
        .'/wp-admin/'
        .$_file_name
        .'" />';
    return $_custom_files."\n";
}
add_action('admin_head', '_register_custom_files');

/**
 * ページネーション
 * @return [type] [description]
 */
function wp_pagination() {
    global $wp_query;
    $big = 99999999;
    $num = 5;
    $prev = '&laquo';
    $next = '&raquo;';

    $page_format = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type'  => 'array',
        'mid_size' => $num,
        'prev_text' => $prev,
        'next_text' => $next
    ) );
    if( is_array($page_format) ) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<div class="pagenation clearfix"><ul>';
        foreach ( $page_format as $page ) {
            echo '<li class="page-numbers">'.$page.'</li>';
        }
        echo '</ul></div>';
    }
    wp_reset_query();
}
remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');

/**
 * ページネーション出力関数
 * $paged : 現在のページ
 * $pages : 全ページ数
 * $range : 左右に何ページ表示するか
 * $show_only : 1ページしかない時に表示するかどうか
 */
function pagination( $pages, $paged, $range = 2, $show_only = false ) {

    $pages = ( int ) $pages;    //float型で渡ってくるので明示的に int型 へ
    $paged = $paged ?: 1;       //get_query_var('paged')をそのまま投げても大丈夫なように

    //表示テキスト
    $prev = '<img src="'. get_template_directory_uri() .'/images/icon_page-arrow_left.png">';
    $next = '<img src="'. get_template_directory_uri() .'/images/icon_page-arrow_right.png">';

    if ( $pages === 1 ) return;    // １ページのみで表示設定もない場合

    if ( 1 !== $pages ) {
        //２ページ以上の時
        echo '<div class="pager-number"><ul class="pager__list">';
        if ( $paged > $range + 1 ) {
            // 「最初へ」 の表示
            echo '<li class="pager__list-item"><a href="' . get_pagenum_link(1) .'">' . $prev . '</a></li>';
        }
        for ( $i = 1; $i <= $pages; $i++ ) {
            if ( $i <= $paged + $range && $i >= $paged - $range ) {
                // $paged +- $range 以内であればページ番号を出力
                if ( $paged === $i ) {
                    echo '<li class="pager__list-item"><span class="current">'. $i .'</span></li>';
                } else {
                    echo '<li class="pager__list-item"><a href="' . get_pagenum_link( $i ) . '">'. $i .'</a></li>';
                }
            }

        }
        if ( $paged + $range < $pages ) {
            // 「最後へ」 の表示
            echo '<li class="pager__list-item"><a href="'. get_pagenum_link( $pages ) .'" class="last">'. $next .'</a></li>';
        }
        echo '</ul></div>';
    }
}

//概要（抜粋）の文字数調整
function my_excerpt_length($length) {
    return 40;
}
add_filter('excerpt_mblength', 'my_excerpt_length');

function my_excerpt_more($post) {
    return '<span class="more">. . .</span>';
}
add_filter('excerpt_more', 'my_excerpt_more');

// contactform7 メールアドレス確認用
add_filter( 'wpcf7_validate_email', 'wpcf7_text_validation_filter_extend', 11, 2 );
add_filter( 'wpcf7_validate_email*', 'wpcf7_text_validation_filter_extend', 11, 2 );
function wpcf7_text_validation_filter_extend( $result, $tag ) {
global $my_email_confirm;
$tag = new WPCF7_Shortcode( $tag );
$name = $tag->name;
$value = isset( $_POST[$name] )
? trim( wp_unslash( strtr( (string) $_POST[$name], "\n", " " ) ) )
: '';
if ($name == "your-email"){
$my_email_confirm=$value;
}
if ($name == "your-email_confirm" && $my_email_confirm != $value){
$result->invalidate( $tag,"確認用のメールアドレスが一致していません");
}

return $result;
}

// ビジュアルエディタを非表示
//function disable_visual_editor_in_page(){
//    add_filter('user_can_richedit', 'disable_visual_editor_filter');
//}
//function disable_visual_editor_filter(){
//    return false;
//}
//add_action( 'load-post.php', 'disable_visual_editor_in_page' );
//add_action( 'load-post-new.php', 'disable_visual_editor_in_page' );

//管理画面での記事一覧カラムカスタマイズ
//WordpressPoplarPostのビューカウントを表示する
function admin_posts_columns($columns) {
    $columns['subtitle'] = "ビュー";
    return $columns;
}
function add_admincolumn($column_name, $post_id) {
    if( $column_name == 'subtitle' ) {
        echo wpp_get_views($post_id, 'monthly', true);
    }
}
if ( function_exists('wpp_get_views') ) {
    add_filter( 'manage_posts_columns', 'admin_posts_columns' );
    add_action( 'manage_posts_custom_column', 'add_admincolumn', 10, 2 );
}

//画像からリンクを削除
add_filter( 'the_content', 'attachment_image_link_remove_filter' );
function attachment_image_link_remove_filter( $content ) {
    $content =
        preg_replace(
            array('{<a(.*?)(wp-att)[^>]*><img}',
                '{ wp-image-[0-9]*" /></a>}'),
            array('<img','" />'),
            $content
        );
    return $content;
}

// 文字変換を止める クォート（引用符）、アポストロフィ、ダッシュ、省略記号など
remove_filter('the_title', 'wptexturize');
remove_filter('the_content', 'wptexturize');
remove_filter('the_excerpt', 'wptexturize');

// オートフォーマット関連の無効化
add_action('init', function() {
    remove_filter('the_title', 'wptexturize');
    remove_filter('the_content', 'wptexturize');
    remove_filter('the_excerpt', 'wptexturize');
    remove_filter('the_title', 'wpautop');
    remove_filter('the_content', 'wpautop');
    remove_filter('the_excerpt', 'wpautop');
    remove_filter('the_editor_content', 'wp_richedit_pre');
});

// オートフォーマット関連の無効化 TinyMCE
add_filter('tiny_mce_before_init', function($init) {
    $init['wpautop'] = false;
    $init['apply_source_formatting'] = ture;
    return $init;
});

add_action( 'wp_footer', 'add_thanks_page' );
function add_thanks_page() {
    $url  = empty($_SERVER["HTTPS"]) ? "http://" : "https://";
    $url .= $_SERVER["HTTP_HOST"];
?>
<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
  location = '<?php echo $url; ?>/thanks'; /* 遷移先のURL */
}, false );
</script>
<?php
}

