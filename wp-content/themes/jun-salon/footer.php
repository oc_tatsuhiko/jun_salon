<?php
/**
 * The template for displaying the footer
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */

?>
<?php $footer_class = ( is_home() || is_front_page() ) ? 'footer-top' : 'footer'; ?>

<footer class="<?php echo $footer_class; ?>">&copy; 2019 JUN SALON</footer>
