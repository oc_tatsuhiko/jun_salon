(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

$(function () {
	var setGallery;
	var setStyle;

	nextArrow();
	setInterval(nextArrow, 4000);

	$('.arrowRightGallery').hover(function () {
		arrowRightGallery();
		setGallery = setInterval(arrowRightGallery, 4000);
	}, function () {
		clearInterval(setGallery);
	});

	$('.arrowRightStyle').hover(function () {
		arrowRightStyle();
		setStyle = setInterval(arrowRightStyle, 4000);
	}, function () {
		clearInterval(setStyle);
	});
});

function nextArrow() {
	$('.nextArrow').animate({
		top: '0',
		opacity: 1
	}, 1000).animate({
		top: '0',
		opacity: 1
	}, 500).animate({
		top: '26px',
		opacity: 0
	}, 1000).animate({
		top: '-26px',
		opacity: 0
	}, 1000);
}

function arrowRightGallery() {
	$('.arrowRightGallery').animate({
		left: '26px',
		opacity: 0
	}, 1000).animate({
		left: '-26px',
		opacity: 0
	}, 1000).animate({
		left: '0',
		opacity: 1
	}, 1000).animate({
		left: '0',
		opacity: 1
	}, 500);
}

function arrowRightStyle() {
	$('.arrowRightStyle').animate({
		left: '26px',
		opacity: 0
	}, 1000).animate({
		left: '-26px',
		opacity: 0
	}, 1000).animate({
		left: '0',
		opacity: 1
	}, 1000).animate({
		left: '0',
		opacity: 1
	}, 500);
}

},{}],2:[function(require,module,exports){
"use strict";

$(function () {
	var flgs = {
		"__concept": false,
		"__news": false,
		"__gallery": false,
		"__style": false,
		"__access": false
	};
	$('#fullpage').fullpage({
		anchors: ['__top', '__concept', '__gallery', '__style', '__news', '__access'], // ページのアンカー名を指定
		menu: '#globalMenu', // グローバルメニューのID名
		scrollOverflow: true, //全画面よりコンテンツが多い場合スクロールバーを出すかどうか
		touchSensitivity: 5, //タッチデバイスでのセクション切替の強さの閾値
		normalScrollElements: '.nav-slider, .list-access, .sp-modal__wrap',
		normalScrollElementTouchThreshold: 3,
		recordHistory: false, // スクロールした時にブラウザに履歴を残さない
		scrollBar: false,
		autoScrolling: true,
		fitToSection: true,
		fitToSectionDelay: 0,
		afterRender: function afterRender() {},
		//セクションが読み込み終わりのイベント
		afterLoad: function afterLoad(anchorLink, index) {
			if (anchorLink === '__top') {
				// ヘッダーのロゴを非表示
				$('#headerLogo').css({ opacity: 0 });

				setTimeout(function () {
					$('#topLogo').css({ opacity: 1 });
				}, 1800);
				setTimeout(function () {
					$('#topArrow').css({ opacity: 1 });
				}, 2100);
			} else {
				// ヘッダーのロゴを表示
				$('#headerLogo').css({ opacity: 1 });

				// 各ページの表示設定
				if (flgs[anchorLink] === false) {
					flgs[anchorLink] = true;
					if (anchorLink === '__access') {
						slideIn(anchorLink);
						setTimeout(function () {
							$('.section__access .slideInArrow').animate({
								opacity: 1
							}, 800);
						}, 1800);
					} else {
						slideIn(anchorLink);
					}
				}
			}

			// nav 下線バーの処理
			navAnchor(index);

			// sp用　メニューの下線処理
			$('#navSliderList a.nav-slider__link').removeClass('is-active');
			$('#navSliderList a.nav-slider__link[href="#' + anchorLink + '"]').addClass('is-active');

			// $.fn.fullpage.setAllowScrolling(true);
		},
		// セクションを離れた時のイベント
		onLeave: function onLeave(index, nextIndex, direction) {
			// nav 下線バーの処理
			navAnchor(nextIndex);

			// sp用　メニューの非表示処理
			$(".nav-slider").fadeOut();
			if ($("#menuTrigger").hasClass("is-active")) {
				$("#menuTrigger").removeClass("is-active");
			}

			// TOPから離れる場合はロゴと矢印を非表示にする
			if (index === 1) {
				$('#topLogo').css({ opacity: 0 });
				$('#topArrow').css({ opacity: 0 });
			}
		},
		// リサイズ後のイベント
		afterResize: function afterResize() {
			$.fn.fullpage.reBuild();
		}
	});
});

/**
 * グローバルナビ　アンカー移動
 * @param index fullpage.js リンクオブジェクト
 */
function navAnchor(index) {
	var width = $('.nav__list-item a').eq(index - 1).width();
	var position = $('.nav__list-item a').eq(index - 1).position();
	$('#globalMenuLine').animate({
		width: width,
		left: position.left
	}, 500);
}

function slideIn(anchorLink) {
	$('.section' + anchorLink + ' .slideInLeft').animate({
		width: '100%'
	}, 800);
	setTimeout(function () {
		$('.section' + anchorLink + ' .slideInRight').animate({
			width: '100%'
		}, 800);
	}, 1200);
}

},{}],3:[function(require,module,exports){
'use strict';

$(function () {
	setSectionWidth();
});

//リサイズされたときの処理
$(window).resize(function () {
	setSectionWidth();
});

function setSectionWidth() {
	var sectionArray = ['__concept', '__news', '__access'];
	for (var i = 0; i < sectionArray.length; i++) {
		var width = $(".section" + sectionArray[i] + " .slideInLeftWrap").width();
		$(".section" + sectionArray[i] + " .slideInLeftInner").css({ width: width });

		width = $(".section" + sectionArray[i] + " .slideInRightWrap").width();
		$(".section" + sectionArray[i] + " .slideInRightInner").css({ width: width });
	}

	var screenWidth = $('body').width();

	if (screenWidth < 980) {
		var sectionArray = ['__gallery', '__style'];

		for (var i = 0; i < sectionArray.length; i++) {
			var width = $(".section" + sectionArray[i] + " .slideInLeftWrap").width();
			$(".section" + sectionArray[i] + " .slideInLeftInner").css({ width: width });

			width = $(".section" + sectionArray[i] + " .slideInRightWrap").width();
			$(".section" + sectionArray[i] + " .slideInRightInner").css({ width: width });
		}
	} else {
		$(".section__gallery .slideInLeftInner").css({ width: 420 });
		var width = $('#sliderGallery').width();

		$(".section__style .slideInLeftInner").css({ width: 420 });
		var width = $('#sliderGallery').width();
	}
}

},{}],4:[function(require,module,exports){
'use strict';

$(function () {
	var bodyClass = $("body").attr('id');
	if (bodyClass !== 'home') return false;
	var images = document.getElementsByTagName('img'); // ページ内の画像取得
	var percent = document.getElementById('loadNum'); // パーセントのテキスト部分
	var loadingBg = $('#load'); // ローディング背景
	var imgCount = 0;
	var baseCount = 0;
	var current;

	// 画像の読み込み
	for (var i = 0; i < images.length; i++) {
		var img = new Image(); // 画像の作成
		// 画像読み込み完了したとき
		img.onload = function () {
			imgCount += 1;
		};
		// 画像読み込み失敗したとき
		img.onerror = function () {
			imgCount += 1;
		};
		img.src = images[i].src; // 画像にsrcを指定して読み込み開始
	};

	// ローディング処理
	var nowLoading = setInterval(function () {
		if (baseCount <= imgCount) {
			// baseCountがimgCountを追い抜かないようにする
			// 現在の読み込み具合のパーセントを取得
			current = Math.floor(baseCount / images.length * 100);
			// パーセント表示の書き換え
			percent.innerHTML = current;
			baseCount += 1;
			// 全て読み込んだ時
			// if(baseCount == images.length) {
			if (current === 100) {
				// ローディング要素の非表示
				setTimeout(function () {
					loadingBg.css({ opacity: 0 });
				}, 500);
				setTimeout(function () {
					loadingBg.css({ display: "none" });
				}, 1000);
				// navの表示
				setTimeout(function () {
					$("#navTop, #headerTop").animate({
						opacity: "1",
						marginTop: "0"
					});
				}, 2500);
				// snsの表示
				setTimeout(function () {
					$("#snsTop").animate({
						opacity: "1",
						left: "24px"
					});
				}, 3000);
				// ローディングの終了
				clearInterval(nowLoading);
			}
		}
	}, 5);
});

},{}],5:[function(require,module,exports){
'use strict';

var init = require('./init.js');
var loading = require('./loading.js');
var fullpage = require('./fullpage.js');
var slider = require('./slider.js');
var navSlider = require('./nav-slider.js');
var spModal = require('./sp-modal.js');
var arrow = require('./arrow.js');
var navAnchor = require('./nav-anchor.js');
var masonry = require('./masonry.js');

},{"./arrow.js":1,"./fullpage.js":2,"./init.js":3,"./loading.js":4,"./masonry.js":6,"./nav-anchor.js":7,"./nav-slider.js":8,"./slider.js":9,"./sp-modal.js":10}],6:[function(require,module,exports){
'use strict';

// $(function() {
$(window).on('load', function () {
	$('.masonry').masonry({
		// options
		itemSelector: '.masonry__item',
		percentPosition: true
	});
});

},{}],7:[function(require,module,exports){
"use strict";

$(function () {
	if ($("body").attr("id") === "home") return false;
	if ($('#globalMenu a.is-active').length === 0) return false;
	var width = $('#globalMenu a.is-active').width();
	var position = $('#globalMenu a.is-active').position();
	$('#globalMenuLine').css({
		width: width,
		left: position.left
	}, 500);
});

},{}],8:[function(require,module,exports){
"use strict";

$(function () {
	var bodyId = $("body").attr("id");
	$("#menuTrigger").click(function () {
		if ($(this).hasClass("is-active")) {
			$(".nav-slider").fadeOut();
			// fullpage.jsのスクロールを有効化する
			if (bodyId === "home") {
				$.fn.fullpage.setAllowScrolling(true);
			}
		} else {
			$(".nav-slider").fadeIn();
			// fullpage.jsのスクロールを無効化する
			if (bodyId === "home") {
				$.fn.fullpage.setAllowScrolling(false);
			}
		}
		$(this).toggleClass("is-active");
	});

	$("#navSliderList a").click(function () {
		if (bodyId === "home") {
			$.fn.fullpage.setAllowScrolling(true);
		}
	});
});

},{}],9:[function(require,module,exports){
'use strict';

var swiperGallery = new Swiper('#sliderGallery', {
	slidesPerView: 2,
	// centeredSlides: true,
	spaceBetween: 30,
	height: 470
});

var swiperStyle = new Swiper('#sliderStyle', {
	slidesPerView: 3.3,
	// centeredSlides: true,
	spaceBetween: 30,
	height: 470
});
$('#sliderNextGallery').click(function () {
	swiperGallery.slideNext(500);
});

$('#sliderNextStyle').click(function () {
	swiperStyle.slideNext(500);
});

},{}],10:[function(require,module,exports){
"use strict";

$(function () {
	$("body").on("click", "#openModalConcept", function () {
		$("#conceptModal").fadeIn();
		// fullpage.jsのスクロールを無効化する
		$.fn.fullpage.setAllowScrolling(false);
	});

	$("body").on("click", "#openModalAccess", function () {
		$("#accessModal").fadeIn();
		// fullpage.jsのスクロールを無効化する
		$.fn.fullpage.setAllowScrolling(false);
	});

	$("body").on("click", ".spModalClose", function () {
		$(".sp-modal").fadeOut();
		// fullpage.jsのスクロールを有効化する
		$.fn.fullpage.setAllowScrolling(true);
	});
});

},{}]},{},[5])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJkZXYvc3JjL3NjcmlwdHMvYXJyb3cuanMiLCJkZXYvc3JjL3NjcmlwdHMvZnVsbHBhZ2UuanMiLCJkZXYvc3JjL3NjcmlwdHMvaW5pdC5qcyIsImRldi9zcmMvc2NyaXB0cy9sb2FkaW5nLmpzIiwiZGV2L3NyYy9zY3JpcHRzL21haW4uanMiLCJkZXYvc3JjL3NjcmlwdHMvbWFzb25yeS5qcyIsImRldi9zcmMvc2NyaXB0cy9uYXYtYW5jaG9yLmpzIiwiZGV2L3NyYy9zY3JpcHRzL25hdi1zbGlkZXIuanMiLCJkZXYvc3JjL3NjcmlwdHMvc2xpZGVyLmpzIiwiZGV2L3NyYy9zY3JpcHRzL3NwLW1vZGFsLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQSxFQUFFLFlBQVc7QUFDWixLQUFJLFVBQUo7QUFDQSxLQUFJLFFBQUo7O0FBRUE7QUFDQSxhQUFZLFNBQVosRUFBdUIsSUFBdkI7O0FBRUEsR0FBRSxvQkFBRixFQUF3QixLQUF4QixDQUE4QixZQUFVO0FBQ3ZDO0FBQ0EsZUFBYSxZQUFZLGlCQUFaLEVBQStCLElBQS9CLENBQWI7QUFDQSxFQUhELEVBR0UsWUFBVTtBQUNYLGdCQUFjLFVBQWQ7QUFDQSxFQUxEOztBQU9BLEdBQUUsa0JBQUYsRUFBc0IsS0FBdEIsQ0FBNEIsWUFBVTtBQUNyQztBQUNBLGFBQVcsWUFBWSxlQUFaLEVBQTZCLElBQTdCLENBQVg7QUFDQSxFQUhELEVBR0UsWUFBVTtBQUNYLGdCQUFjLFFBQWQ7QUFDQSxFQUxEO0FBT0EsQ0FyQkQ7O0FBdUJBLFNBQVMsU0FBVCxHQUFxQjtBQUNwQixHQUFFLFlBQUYsRUFBZ0IsT0FBaEIsQ0FBd0I7QUFDdkIsT0FBSyxHQURrQjtBQUV2QixXQUFTO0FBRmMsRUFBeEIsRUFHRyxJQUhILEVBR1MsT0FIVCxDQUdpQjtBQUNoQixPQUFLLEdBRFc7QUFFaEIsV0FBUztBQUZPLEVBSGpCLEVBTUcsR0FOSCxFQU1RLE9BTlIsQ0FNZ0I7QUFDZixPQUFLLE1BRFU7QUFFZixXQUFTO0FBRk0sRUFOaEIsRUFTRyxJQVRILEVBU1MsT0FUVCxDQVNpQjtBQUNoQixPQUFLLE9BRFc7QUFFaEIsV0FBUztBQUZPLEVBVGpCLEVBWUcsSUFaSDtBQWFBOztBQUVELFNBQVMsaUJBQVQsR0FBNkI7QUFDNUIsR0FBRSxvQkFBRixFQUF3QixPQUF4QixDQUFnQztBQUMvQixRQUFNLE1BRHlCO0FBRS9CLFdBQVM7QUFGc0IsRUFBaEMsRUFHRyxJQUhILEVBR1MsT0FIVCxDQUdpQjtBQUNoQixRQUFNLE9BRFU7QUFFaEIsV0FBUztBQUZPLEVBSGpCLEVBTUcsSUFOSCxFQU1TLE9BTlQsQ0FNaUI7QUFDaEIsUUFBTSxHQURVO0FBRWhCLFdBQVM7QUFGTyxFQU5qQixFQVNHLElBVEgsRUFTUyxPQVRULENBU2lCO0FBQ2hCLFFBQU0sR0FEVTtBQUVoQixXQUFTO0FBRk8sRUFUakIsRUFZRyxHQVpIO0FBYUE7O0FBRUQsU0FBUyxlQUFULEdBQTJCO0FBQzFCLEdBQUUsa0JBQUYsRUFBc0IsT0FBdEIsQ0FBOEI7QUFDN0IsUUFBTSxNQUR1QjtBQUU3QixXQUFTO0FBRm9CLEVBQTlCLEVBR0csSUFISCxFQUdTLE9BSFQsQ0FHaUI7QUFDaEIsUUFBTSxPQURVO0FBRWhCLFdBQVM7QUFGTyxFQUhqQixFQU1HLElBTkgsRUFNUyxPQU5ULENBTWlCO0FBQ2hCLFFBQU0sR0FEVTtBQUVoQixXQUFTO0FBRk8sRUFOakIsRUFTRyxJQVRILEVBU1MsT0FUVCxDQVNpQjtBQUNoQixRQUFNLEdBRFU7QUFFaEIsV0FBUztBQUZPLEVBVGpCLEVBWUcsR0FaSDtBQWFBOzs7OztBQ3JFRCxFQUFFLFlBQVc7QUFDWixLQUFJLE9BQU87QUFDVixlQUFhLEtBREg7QUFFVixZQUFVLEtBRkE7QUFHVixlQUFhLEtBSEg7QUFJVixhQUFXLEtBSkQ7QUFLVixjQUFZO0FBTEYsRUFBWDtBQU9BLEdBQUUsV0FBRixFQUFlLFFBQWYsQ0FBd0I7QUFDdkI7QUFDQSxXQUFTLENBQUMsT0FBRCxFQUFVLFdBQVYsRUFBdUIsV0FBdkIsRUFBb0MsUUFBcEMsRUFBOEMsVUFBOUMsQ0FGYyxFQUU2QztBQUNwRSxRQUFNLGFBSGlCLEVBR0Y7QUFDckIsa0JBQWdCLElBSk8sRUFJRDtBQUN0QixvQkFBa0IsQ0FMSyxFQUtGO0FBQ3JCLHdCQUFzQiw0Q0FOQztBQU92QixxQ0FBbUMsQ0FQWjtBQVF2QixpQkFBZSxLQVJRLEVBUUQ7QUFDdEIsYUFBVyxJQVRZO0FBVXZCLGlCQUFlLElBVlE7QUFXdkIsZ0JBQWMsSUFYUztBQVl2QixxQkFBbUIsQ0FaSTtBQWF2QixlQUFhLHVCQUFVLENBQUUsQ0FiRjtBQWN2QjtBQUNBLGFBQVcsbUJBQVUsVUFBVixFQUFzQixLQUF0QixFQUE2QjtBQUN2QyxPQUFJLGVBQWUsT0FBbkIsRUFBNEI7QUFDM0I7QUFDQSxNQUFFLGFBQUYsRUFBaUIsR0FBakIsQ0FBcUIsRUFBQyxTQUFTLENBQVYsRUFBckI7O0FBRUEsZUFBWSxZQUFVO0FBQ3JCLE9BQUUsVUFBRixFQUFjLEdBQWQsQ0FBa0IsRUFBQyxTQUFTLENBQVYsRUFBbEI7QUFDQSxLQUZELEVBRUcsSUFGSDtBQUdBLGVBQVksWUFBVTtBQUNyQixPQUFFLFdBQUYsRUFBZSxHQUFmLENBQW1CLEVBQUMsU0FBUyxDQUFWLEVBQW5CO0FBQ0EsS0FGRCxFQUVHLElBRkg7QUFHQSxJQVZELE1BVU87QUFDTjtBQUNBLE1BQUUsYUFBRixFQUFpQixHQUFqQixDQUFxQixFQUFDLFNBQVMsQ0FBVixFQUFyQjs7QUFFQTtBQUNBLFFBQUksS0FBSyxVQUFMLE1BQXFCLEtBQXpCLEVBQWlDO0FBQ2hDLFVBQUssVUFBTCxJQUFtQixJQUFuQjtBQUNBLFNBQUksZUFBZSxVQUFuQixFQUErQjtBQUM5QixjQUFRLFVBQVI7QUFDQSxpQkFBWSxZQUFVO0FBQ3JCLFNBQUUsZ0NBQUYsRUFBb0MsT0FBcEMsQ0FBNEM7QUFDM0MsaUJBQVM7QUFEa0MsUUFBNUMsRUFFRyxHQUZIO0FBR0EsT0FKRCxFQUlHLElBSkg7QUFLQSxNQVBELE1BT087QUFDTixjQUFRLFVBQVI7QUFDQTtBQUNEO0FBQ0Q7O0FBRUQ7QUFDQSxhQUFVLEtBQVY7O0FBRUE7QUFDQSxLQUFFLG1DQUFGLEVBQXVDLFdBQXZDLENBQW1ELFdBQW5EO0FBQ0EsS0FBRSw4Q0FBOEMsVUFBOUMsR0FBMkQsSUFBN0QsRUFBbUUsUUFBbkUsQ0FBNEUsV0FBNUU7O0FBRUE7QUFDQSxHQXREc0I7QUF1RHZCO0FBQ0EsV0FBUyxpQkFBVSxLQUFWLEVBQWlCLFNBQWpCLEVBQTRCLFNBQTVCLEVBQXVDO0FBQy9DO0FBQ0EsYUFBVSxTQUFWOztBQUVBO0FBQ0EsS0FBRSxhQUFGLEVBQWlCLE9BQWpCO0FBQ0EsT0FBSSxFQUFFLGNBQUYsRUFBa0IsUUFBbEIsQ0FBMkIsV0FBM0IsQ0FBSixFQUE2QztBQUM1QyxNQUFFLGNBQUYsRUFBa0IsV0FBbEIsQ0FBOEIsV0FBOUI7QUFDQTs7QUFFRDtBQUNBLE9BQUksVUFBVSxDQUFkLEVBQWlCO0FBQ2hCLE1BQUUsVUFBRixFQUFjLEdBQWQsQ0FBa0IsRUFBQyxTQUFTLENBQVYsRUFBbEI7QUFDQSxNQUFFLFdBQUYsRUFBZSxHQUFmLENBQW1CLEVBQUMsU0FBUyxDQUFWLEVBQW5CO0FBRUE7QUFDRCxHQXhFc0I7QUF5RXZCO0FBQ0EsZUFBYSx1QkFBWTtBQUN4QixLQUFFLEVBQUYsQ0FBSyxRQUFMLENBQWMsT0FBZDtBQUNBO0FBNUVzQixFQUF4QjtBQThFQSxDQXRGRDs7QUF5RkE7Ozs7QUFJQSxTQUFTLFNBQVQsQ0FBbUIsS0FBbkIsRUFBMEI7QUFDekIsS0FBSSxRQUFRLEVBQUUsbUJBQUYsRUFBdUIsRUFBdkIsQ0FBMEIsUUFBTyxDQUFqQyxFQUFvQyxLQUFwQyxFQUFaO0FBQ0EsS0FBSSxXQUFXLEVBQUUsbUJBQUYsRUFBdUIsRUFBdkIsQ0FBMEIsUUFBTyxDQUFqQyxFQUFvQyxRQUFwQyxFQUFmO0FBQ0EsR0FBRSxpQkFBRixFQUFxQixPQUFyQixDQUE2QjtBQUM1QixTQUFPLEtBRHFCO0FBRTVCLFFBQU0sU0FBUztBQUZhLEVBQTdCLEVBR0UsR0FIRjtBQUlBOztBQUVELFNBQVMsT0FBVCxDQUFpQixVQUFqQixFQUE0QjtBQUMzQixHQUFFLGFBQWEsVUFBYixHQUEwQixlQUE1QixFQUE2QyxPQUE3QyxDQUFxRDtBQUNwRCxTQUFPO0FBRDZDLEVBQXJELEVBRUUsR0FGRjtBQUdBLFlBQVksWUFBVTtBQUNyQixJQUFFLGFBQWEsVUFBYixHQUEwQixnQkFBNUIsRUFBOEMsT0FBOUMsQ0FBc0Q7QUFDckQsVUFBTztBQUQ4QyxHQUF0RCxFQUVHLEdBRkg7QUFHQSxFQUpELEVBSUcsSUFKSDtBQUtBOzs7OztBQy9HRCxFQUFFLFlBQVc7QUFDWjtBQUNBLENBRkQ7O0FBSUE7QUFDQSxFQUFFLE1BQUYsRUFBVSxNQUFWLENBQWlCLFlBQVc7QUFDM0I7QUFDQSxDQUZEOztBQUlBLFNBQVMsZUFBVCxHQUEwQjtBQUN6QixLQUFJLGVBQWUsQ0FBQyxXQUFELEVBQWMsUUFBZCxFQUF3QixVQUF4QixDQUFuQjtBQUNBLE1BQUssSUFBSSxJQUFJLENBQWIsRUFBZ0IsSUFBSSxhQUFhLE1BQWpDLEVBQXlDLEdBQXpDLEVBQThDO0FBQzdDLE1BQUksUUFBUSxFQUFFLGFBQWEsYUFBYSxDQUFiLENBQWIsR0FBK0IsbUJBQWpDLEVBQXNELEtBQXRELEVBQVo7QUFDQSxJQUFFLGFBQWEsYUFBYSxDQUFiLENBQWIsR0FBK0Isb0JBQWpDLEVBQXVELEdBQXZELENBQTJELEVBQUMsT0FBTyxLQUFSLEVBQTNEOztBQUVBLFVBQVEsRUFBRSxhQUFhLGFBQWEsQ0FBYixDQUFiLEdBQStCLG9CQUFqQyxFQUF1RCxLQUF2RCxFQUFSO0FBQ0EsSUFBRSxhQUFhLGFBQWEsQ0FBYixDQUFiLEdBQStCLHFCQUFqQyxFQUF3RCxHQUF4RCxDQUE0RCxFQUFDLE9BQU8sS0FBUixFQUE1RDtBQUNBOztBQUVELEtBQUksY0FBYyxFQUFFLE1BQUYsRUFBVSxLQUFWLEVBQWxCOztBQUVBLEtBQUksY0FBYyxHQUFsQixFQUF1QjtBQUN0QixNQUFJLGVBQWUsQ0FBQyxXQUFELEVBQWMsU0FBZCxDQUFuQjs7QUFFQSxPQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksYUFBYSxNQUFqQyxFQUF5QyxHQUF6QyxFQUE4QztBQUM3QyxPQUFJLFFBQVEsRUFBRSxhQUFhLGFBQWEsQ0FBYixDQUFiLEdBQStCLG1CQUFqQyxFQUFzRCxLQUF0RCxFQUFaO0FBQ0EsS0FBRSxhQUFhLGFBQWEsQ0FBYixDQUFiLEdBQStCLG9CQUFqQyxFQUF1RCxHQUF2RCxDQUEyRCxFQUFDLE9BQU8sS0FBUixFQUEzRDs7QUFFQSxXQUFRLEVBQUUsYUFBYSxhQUFhLENBQWIsQ0FBYixHQUErQixvQkFBakMsRUFBdUQsS0FBdkQsRUFBUjtBQUNBLEtBQUUsYUFBYSxhQUFhLENBQWIsQ0FBYixHQUErQixxQkFBakMsRUFBd0QsR0FBeEQsQ0FBNEQsRUFBQyxPQUFPLEtBQVIsRUFBNUQ7QUFDQTtBQUVELEVBWEQsTUFXTztBQUNOLElBQUUscUNBQUYsRUFBeUMsR0FBekMsQ0FBNkMsRUFBQyxPQUFPLEdBQVIsRUFBN0M7QUFDQSxNQUFJLFFBQVEsRUFBRSxnQkFBRixFQUFvQixLQUFwQixFQUFaOztBQUVBLElBQUUsbUNBQUYsRUFBdUMsR0FBdkMsQ0FBMkMsRUFBQyxPQUFPLEdBQVIsRUFBM0M7QUFDQSxNQUFJLFFBQVEsRUFBRSxnQkFBRixFQUFvQixLQUFwQixFQUFaO0FBQ0E7QUFDRDs7Ozs7QUN2Q0QsRUFBRSxZQUFXO0FBQ1osS0FBSSxZQUFZLEVBQUUsTUFBRixFQUFVLElBQVYsQ0FBZSxJQUFmLENBQWhCO0FBQ0EsS0FBSSxjQUFjLE1BQWxCLEVBQTBCLE9BQU8sS0FBUDtBQUMxQixLQUFJLFNBQVMsU0FBUyxvQkFBVCxDQUE4QixLQUE5QixDQUFiLENBSFksQ0FHdUM7QUFDbkQsS0FBSSxVQUFVLFNBQVMsY0FBVCxDQUF3QixTQUF4QixDQUFkLENBSlksQ0FJc0M7QUFDbEQsS0FBSSxZQUFZLEVBQUUsT0FBRixDQUFoQixDQUxZLENBS2dCO0FBQzVCLEtBQUksV0FBVyxDQUFmO0FBQ0EsS0FBSSxZQUFZLENBQWhCO0FBQ0EsS0FBSSxPQUFKOztBQUVBO0FBQ0EsTUFBSyxJQUFJLElBQUksQ0FBYixFQUFnQixJQUFJLE9BQU8sTUFBM0IsRUFBbUMsR0FBbkMsRUFBd0M7QUFDdkMsTUFBSSxNQUFNLElBQUksS0FBSixFQUFWLENBRHVDLENBQ2hCO0FBQ3ZCO0FBQ0EsTUFBSSxNQUFKLEdBQWEsWUFBVztBQUN2QixlQUFZLENBQVo7QUFDQSxHQUZEO0FBR0E7QUFDQSxNQUFJLE9BQUosR0FBYyxZQUFXO0FBQ3hCLGVBQVksQ0FBWjtBQUNBLEdBRkQ7QUFHQSxNQUFJLEdBQUosR0FBVSxPQUFPLENBQVAsRUFBVSxHQUFwQixDQVZ1QyxDQVVkO0FBQ3pCOztBQUVEO0FBQ0EsS0FBSSxhQUFhLFlBQVksWUFBVztBQUN2QyxNQUFHLGFBQWEsUUFBaEIsRUFBMEI7QUFBRTtBQUMzQjtBQUNBLGFBQVUsS0FBSyxLQUFMLENBQVcsWUFBWSxPQUFPLE1BQW5CLEdBQTRCLEdBQXZDLENBQVY7QUFDQTtBQUNBLFdBQVEsU0FBUixHQUFvQixPQUFwQjtBQUNBLGdCQUFhLENBQWI7QUFDQTtBQUNBO0FBQ0EsT0FBRyxZQUFZLEdBQWYsRUFBb0I7QUFDbkI7QUFDQSxlQUFZLFlBQVU7QUFDckIsZUFBVSxHQUFWLENBQWMsRUFBQyxTQUFTLENBQVYsRUFBZDtBQUNBLEtBRkQsRUFFRyxHQUZIO0FBR0EsZUFBWSxZQUFVO0FBQ3JCLGVBQVUsR0FBVixDQUFjLEVBQUMsU0FBUyxNQUFWLEVBQWQ7QUFDQSxLQUZELEVBRUcsSUFGSDtBQUdBO0FBQ0EsZUFBWSxZQUFVO0FBQ3JCLE9BQUUscUJBQUYsRUFBeUIsT0FBekIsQ0FBaUM7QUFDaEMsZUFBUyxHQUR1QjtBQUVoQyxpQkFBVztBQUZxQixNQUFqQztBQUlBLEtBTEQsRUFLRyxJQUxIO0FBTUE7QUFDQSxlQUFZLFlBQVU7QUFDckIsT0FBRSxTQUFGLEVBQWEsT0FBYixDQUFxQjtBQUNwQixlQUFTLEdBRFc7QUFFcEIsWUFBTTtBQUZjLE1BQXJCO0FBSUEsS0FMRCxFQUtHLElBTEg7QUFNQTtBQUNBLGtCQUFjLFVBQWQ7QUFDQTtBQUVEO0FBQ0QsRUFwQ2dCLEVBb0NkLENBcENjLENBQWpCO0FBcUNBLENBOUREOzs7OztBQ0FBLElBQUksT0FBTyxRQUFTLFdBQVQsQ0FBWDtBQUNBLElBQUksVUFBVSxRQUFTLGNBQVQsQ0FBZDtBQUNBLElBQUksV0FBVyxRQUFTLGVBQVQsQ0FBZjtBQUNBLElBQUksU0FBUyxRQUFTLGFBQVQsQ0FBYjtBQUNBLElBQUksWUFBWSxRQUFTLGlCQUFULENBQWhCO0FBQ0EsSUFBSSxVQUFVLFFBQVMsZUFBVCxDQUFkO0FBQ0EsSUFBSSxRQUFRLFFBQVMsWUFBVCxDQUFaO0FBQ0EsSUFBSSxZQUFZLFFBQVMsaUJBQVQsQ0FBaEI7QUFDQSxJQUFJLFVBQVUsUUFBUyxjQUFULENBQWQ7Ozs7O0FDUkE7QUFDQSxFQUFFLE1BQUYsRUFBVSxFQUFWLENBQWEsTUFBYixFQUFxQixZQUFVO0FBQzlCLEdBQUUsVUFBRixFQUFjLE9BQWQsQ0FBc0I7QUFDckI7QUFDQSxnQkFBYyxnQkFGTztBQUdyQixtQkFBaUI7QUFISSxFQUF0QjtBQUtBLENBTkQ7Ozs7O0FDREEsRUFBRSxZQUFXO0FBQ1osS0FBSSxFQUFFLE1BQUYsRUFBVSxJQUFWLENBQWUsSUFBZixNQUF5QixNQUE3QixFQUFxQyxPQUFPLEtBQVA7QUFDckMsS0FBSSxRQUFRLEVBQUUseUJBQUYsRUFBNkIsS0FBN0IsRUFBWjtBQUNBLEtBQUksV0FBVyxFQUFFLHlCQUFGLEVBQTZCLFFBQTdCLEVBQWY7QUFDQSxHQUFFLGlCQUFGLEVBQXFCLEdBQXJCLENBQXlCO0FBQ3hCLFNBQU8sS0FEaUI7QUFFeEIsUUFBTSxTQUFTO0FBRlMsRUFBekIsRUFHRSxHQUhGO0FBSUEsQ0FSRDs7Ozs7QUNBQSxFQUFFLFlBQVc7QUFDWixLQUFJLFNBQVMsRUFBRSxNQUFGLEVBQVUsSUFBVixDQUFlLElBQWYsQ0FBYjtBQUNBLEdBQUUsY0FBRixFQUFrQixLQUFsQixDQUF3QixZQUFZO0FBQ25DLE1BQUksRUFBRSxJQUFGLEVBQVEsUUFBUixDQUFpQixXQUFqQixDQUFKLEVBQW1DO0FBQ2xDLEtBQUUsYUFBRixFQUFpQixPQUFqQjtBQUNBO0FBQ0EsT0FBSSxXQUFXLE1BQWYsRUFBdUI7QUFDdEIsTUFBRSxFQUFGLENBQUssUUFBTCxDQUFjLGlCQUFkLENBQWdDLElBQWhDO0FBQ0E7QUFDRCxHQU5ELE1BTU87QUFDTixLQUFFLGFBQUYsRUFBaUIsTUFBakI7QUFDQTtBQUNBLE9BQUksV0FBVyxNQUFmLEVBQXVCO0FBQ3RCLE1BQUUsRUFBRixDQUFLLFFBQUwsQ0FBYyxpQkFBZCxDQUFnQyxLQUFoQztBQUNBO0FBQ0Q7QUFDRCxJQUFFLElBQUYsRUFBUSxXQUFSLENBQW9CLFdBQXBCO0FBQ0EsRUFmRDs7QUFpQkEsR0FBRSxrQkFBRixFQUFzQixLQUF0QixDQUE0QixZQUFZO0FBQ3ZDLE1BQUksV0FBVyxNQUFmLEVBQXVCO0FBQ3RCLEtBQUUsRUFBRixDQUFLLFFBQUwsQ0FBYyxpQkFBZCxDQUFnQyxJQUFoQztBQUNBO0FBQ0QsRUFKRDtBQUtBLENBeEJEOzs7OztBQ0FBLElBQUksZ0JBQWdCLElBQUksTUFBSixDQUFXLGdCQUFYLEVBQTZCO0FBQ2hELGdCQUFlLENBRGlDO0FBRWhEO0FBQ0EsZUFBYyxFQUhrQztBQUloRCxTQUFRO0FBSndDLENBQTdCLENBQXBCOztBQU9BLElBQUksY0FBYyxJQUFJLE1BQUosQ0FBVyxjQUFYLEVBQTJCO0FBQzVDLGdCQUFlLEdBRDZCO0FBRTVDO0FBQ0EsZUFBYyxFQUg4QjtBQUk1QyxTQUFRO0FBSm9DLENBQTNCLENBQWxCO0FBTUEsRUFBRSxvQkFBRixFQUF3QixLQUF4QixDQUE4QixZQUFXO0FBQ3hDLGVBQWMsU0FBZCxDQUF3QixHQUF4QjtBQUNBLENBRkQ7O0FBSUEsRUFBRSxrQkFBRixFQUFzQixLQUF0QixDQUE0QixZQUFXO0FBQ3RDLGFBQVksU0FBWixDQUFzQixHQUF0QjtBQUNBLENBRkQ7Ozs7O0FDakJBLEVBQUUsWUFBVztBQUNaLEdBQUUsTUFBRixFQUFVLEVBQVYsQ0FBYSxPQUFiLEVBQXNCLG1CQUF0QixFQUEyQyxZQUFZO0FBQ3RELElBQUUsZUFBRixFQUFtQixNQUFuQjtBQUNBO0FBQ0EsSUFBRSxFQUFGLENBQUssUUFBTCxDQUFjLGlCQUFkLENBQWdDLEtBQWhDO0FBQ0EsRUFKRDs7QUFNQSxHQUFFLE1BQUYsRUFBVSxFQUFWLENBQWEsT0FBYixFQUFzQixrQkFBdEIsRUFBMEMsWUFBWTtBQUNyRCxJQUFFLGNBQUYsRUFBa0IsTUFBbEI7QUFDQTtBQUNBLElBQUUsRUFBRixDQUFLLFFBQUwsQ0FBYyxpQkFBZCxDQUFnQyxLQUFoQztBQUNBLEVBSkQ7O0FBTUEsR0FBRSxNQUFGLEVBQVUsRUFBVixDQUFhLE9BQWIsRUFBc0IsZUFBdEIsRUFBdUMsWUFBWTtBQUNsRCxJQUFFLFdBQUYsRUFBZSxPQUFmO0FBQ0E7QUFDQSxJQUFFLEVBQUYsQ0FBSyxRQUFMLENBQWMsaUJBQWQsQ0FBZ0MsSUFBaEM7QUFDQSxFQUpEO0FBS0EsQ0FsQkQiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCIkKGZ1bmN0aW9uKCkge1xuXHR2YXIgc2V0R2FsbGVyeTtcblx0dmFyIHNldFN0eWxlO1xuXG5cdG5leHRBcnJvdygpO1xuXHRzZXRJbnRlcnZhbChuZXh0QXJyb3csIDQwMDApO1xuXG5cdCQoJy5hcnJvd1JpZ2h0R2FsbGVyeScpLmhvdmVyKGZ1bmN0aW9uKCl7XG5cdFx0YXJyb3dSaWdodEdhbGxlcnkoKTtcblx0XHRzZXRHYWxsZXJ5ID0gc2V0SW50ZXJ2YWwoYXJyb3dSaWdodEdhbGxlcnksIDQwMDApO1xuXHR9LGZ1bmN0aW9uKCl7XG5cdFx0Y2xlYXJJbnRlcnZhbChzZXRHYWxsZXJ5KTtcblx0fSk7XG5cblx0JCgnLmFycm93UmlnaHRTdHlsZScpLmhvdmVyKGZ1bmN0aW9uKCl7XG5cdFx0YXJyb3dSaWdodFN0eWxlKCk7XG5cdFx0c2V0U3R5bGUgPSBzZXRJbnRlcnZhbChhcnJvd1JpZ2h0U3R5bGUsIDQwMDApO1xuXHR9LGZ1bmN0aW9uKCl7XG5cdFx0Y2xlYXJJbnRlcnZhbChzZXRTdHlsZSk7XG5cdH0pO1xuXG59KTtcblxuZnVuY3Rpb24gbmV4dEFycm93KCkge1xuXHQkKCcubmV4dEFycm93JykuYW5pbWF0ZSh7XG5cdFx0dG9wOiAnMCcsXG5cdFx0b3BhY2l0eTogMVxuXHR9LCAxMDAwKS5hbmltYXRlKHtcblx0XHR0b3A6ICcwJyxcblx0XHRvcGFjaXR5OiAxXG5cdH0sIDUwMCkuYW5pbWF0ZSh7XG5cdFx0dG9wOiAnMjZweCcsXG5cdFx0b3BhY2l0eTogMFxuXHR9LCAxMDAwKS5hbmltYXRlKHtcblx0XHR0b3A6ICctMjZweCcsXG5cdFx0b3BhY2l0eTogMFxuXHR9LCAxMDAwKTtcbn1cblxuZnVuY3Rpb24gYXJyb3dSaWdodEdhbGxlcnkoKSB7XG5cdCQoJy5hcnJvd1JpZ2h0R2FsbGVyeScpLmFuaW1hdGUoe1xuXHRcdGxlZnQ6ICcyNnB4Jyxcblx0XHRvcGFjaXR5OiAwXG5cdH0sIDEwMDApLmFuaW1hdGUoe1xuXHRcdGxlZnQ6ICctMjZweCcsXG5cdFx0b3BhY2l0eTogMFxuXHR9LCAxMDAwKS5hbmltYXRlKHtcblx0XHRsZWZ0OiAnMCcsXG5cdFx0b3BhY2l0eTogMVxuXHR9LCAxMDAwKS5hbmltYXRlKHtcblx0XHRsZWZ0OiAnMCcsXG5cdFx0b3BhY2l0eTogMVxuXHR9LCA1MDApO1xufVxuXG5mdW5jdGlvbiBhcnJvd1JpZ2h0U3R5bGUoKSB7XG5cdCQoJy5hcnJvd1JpZ2h0U3R5bGUnKS5hbmltYXRlKHtcblx0XHRsZWZ0OiAnMjZweCcsXG5cdFx0b3BhY2l0eTogMFxuXHR9LCAxMDAwKS5hbmltYXRlKHtcblx0XHRsZWZ0OiAnLTI2cHgnLFxuXHRcdG9wYWNpdHk6IDBcblx0fSwgMTAwMCkuYW5pbWF0ZSh7XG5cdFx0bGVmdDogJzAnLFxuXHRcdG9wYWNpdHk6IDFcblx0fSwgMTAwMCkuYW5pbWF0ZSh7XG5cdFx0bGVmdDogJzAnLFxuXHRcdG9wYWNpdHk6IDFcblx0fSwgNTAwKTtcbn1cbiIsIiQoZnVuY3Rpb24oKSB7XG5cdHZhciBmbGdzID0ge1xuXHRcdFwiX19jb25jZXB0XCI6IGZhbHNlLFxuXHRcdFwiX19uZXdzXCI6IGZhbHNlLFxuXHRcdFwiX19nYWxsZXJ5XCI6IGZhbHNlLFxuXHRcdFwiX19zdHlsZVwiOiBmYWxzZSxcblx0XHRcIl9fYWNjZXNzXCI6IGZhbHNlLFxuXHR9XG5cdCQoJyNmdWxscGFnZScpLmZ1bGxwYWdlKHtcblx0XHQvLyBhbmNob3JzOiBbJ19fdG9wJywgJ19fY29uY2VwdCcsICdfX2dhbGxlcnknLCAnX19zdHlsZScsICdfX25ld3MnLCAnX19hY2Nlc3MnXSwgLy8g44Oa44O844K444Gu44Ki44Oz44Kr44O85ZCN44KS5oyH5a6aXG5cdFx0YW5jaG9yczogWydfX3RvcCcsICdfX2NvbmNlcHQnLCAnX19nYWxsZXJ5JywgJ19fbmV3cycsICdfX2FjY2VzcyddLCAvLyDjg5rjg7zjgrjjga7jgqLjg7Pjgqvjg7zlkI3jgpLmjIflrppcblx0XHRtZW51OiAnI2dsb2JhbE1lbnUnLCAvLyDjgrDjg63jg7zjg5Djg6vjg6Hjg4vjg6Xjg7zjga5JROWQjVxuXHRcdHNjcm9sbE92ZXJmbG93OiB0cnVlLCAvL+WFqOeUu+mdouOCiOOCiuOCs+ODs+ODhuODs+ODhOOBjOWkmuOBhOWgtOWQiOOCueOCr+ODreODvOODq+ODkOODvOOCkuWHuuOBmeOBi+OBqeOBhuOBi1xuXHRcdHRvdWNoU2Vuc2l0aXZpdHk6IDUsIC8v44K/44OD44OB44OH44OQ44Kk44K544Gn44Gu44K744Kv44K344On44Oz5YiH5pu/44Gu5by344GV44Gu6Za+5YCkXG5cdFx0bm9ybWFsU2Nyb2xsRWxlbWVudHM6ICcubmF2LXNsaWRlciwgLmxpc3QtYWNjZXNzLCAuc3AtbW9kYWxfX3dyYXAnLFxuXHRcdG5vcm1hbFNjcm9sbEVsZW1lbnRUb3VjaFRocmVzaG9sZDogMyxcblx0XHRyZWNvcmRIaXN0b3J5OiBmYWxzZSwgLy8g44K544Kv44Ot44O844Or44GX44Gf5pmC44Gr44OW44Op44Km44K244Gr5bGl5q2044KS5q6L44GV44Gq44GEXG5cdFx0c2Nyb2xsQmFyOiB0cnVlLFxuXHRcdGF1dG9TY3JvbGxpbmc6IHRydWUsXG5cdFx0Zml0VG9TZWN0aW9uOiB0cnVlLFxuXHRcdGZpdFRvU2VjdGlvbkRlbGF5OiAwLFxuXHRcdGFmdGVyUmVuZGVyOiBmdW5jdGlvbigpe30sXG5cdFx0Ly/jgrvjgq/jgrfjg6fjg7PjgYzoqq3jgb/ovrzjgb/ntYLjgo/jgorjga7jgqTjg5njg7Pjg4hcblx0XHRhZnRlckxvYWQ6IGZ1bmN0aW9uIChhbmNob3JMaW5rLCBpbmRleCkge1xuXHRcdFx0aWYgKGFuY2hvckxpbmsgPT09ICdfX3RvcCcpIHtcblx0XHRcdFx0Ly8g44OY44OD44OA44O844Gu44Ot44K044KS6Z2e6KGo56S6XG5cdFx0XHRcdCQoJyNoZWFkZXJMb2dvJykuY3NzKHtvcGFjaXR5OiAwfSk7XG5cblx0XHRcdFx0c2V0VGltZW91dCggZnVuY3Rpb24oKXtcblx0XHRcdFx0XHQkKCcjdG9wTG9nbycpLmNzcyh7b3BhY2l0eTogMX0pO1xuXHRcdFx0XHR9LCAxODAwKTtcblx0XHRcdFx0c2V0VGltZW91dCggZnVuY3Rpb24oKXtcblx0XHRcdFx0XHQkKCcjdG9wQXJyb3cnKS5jc3Moe29wYWNpdHk6IDF9KTtcblx0XHRcdFx0fSwgMjEwMCApO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Ly8g44OY44OD44OA44O844Gu44Ot44K044KS6KGo56S6XG5cdFx0XHRcdCQoJyNoZWFkZXJMb2dvJykuY3NzKHtvcGFjaXR5OiAxfSk7XG5cblx0XHRcdFx0Ly8g5ZCE44Oa44O844K444Gu6KGo56S66Kit5a6aXG5cdFx0XHRcdGlmIChmbGdzW2FuY2hvckxpbmtdID09PSBmYWxzZSkgIHtcblx0XHRcdFx0XHRmbGdzW2FuY2hvckxpbmtdID0gdHJ1ZTtcblx0XHRcdFx0XHRpZiAoYW5jaG9yTGluayA9PT0gJ19fYWNjZXNzJykge1xuXHRcdFx0XHRcdFx0c2xpZGVJbihhbmNob3JMaW5rKTtcblx0XHRcdFx0XHRcdHNldFRpbWVvdXQoIGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0XHRcdCQoJy5zZWN0aW9uX19hY2Nlc3MgLnNsaWRlSW5BcnJvdycpLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0XHRcdG9wYWNpdHk6IDEsXG5cdFx0XHRcdFx0XHRcdH0sIDgwMCk7XG5cdFx0XHRcdFx0XHR9LCAxODAwICk7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHNsaWRlSW4oYW5jaG9yTGluayk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdC8vIG5hdiDkuIvnt5rjg5Djg7zjga7lh6bnkIZcblx0XHRcdG5hdkFuY2hvcihpbmRleCk7XG5cblx0XHRcdC8vIHNw55So44CA44Oh44OL44Ol44O844Gu5LiL57ea5Yem55CGXG5cdFx0XHQkKCcjbmF2U2xpZGVyTGlzdCBhLm5hdi1zbGlkZXJfX2xpbmsnKS5yZW1vdmVDbGFzcygnaXMtYWN0aXZlJyk7XG5cdFx0XHQkKCcjbmF2U2xpZGVyTGlzdCBhLm5hdi1zbGlkZXJfX2xpbmtbaHJlZj1cIiMnICsgYW5jaG9yTGluayArICdcIl0nKS5hZGRDbGFzcygnaXMtYWN0aXZlJyk7XG5cblx0XHRcdC8vICQuZm4uZnVsbHBhZ2Uuc2V0QWxsb3dTY3JvbGxpbmcodHJ1ZSk7XG5cdFx0fSxcblx0XHQvLyDjgrvjgq/jgrfjg6fjg7PjgpLpm6LjgozjgZ/mmYLjga7jgqTjg5njg7Pjg4hcblx0XHRvbkxlYXZlOiBmdW5jdGlvbiAoaW5kZXgsIG5leHRJbmRleCwgZGlyZWN0aW9uKSB7XG5cdFx0XHQvLyBuYXYg5LiL57ea44OQ44O844Gu5Yem55CGXG5cdFx0XHRuYXZBbmNob3IobmV4dEluZGV4KTtcblxuXHRcdFx0Ly8gc3DnlKjjgIDjg6Hjg4vjg6Xjg7zjga7pnZ7ooajnpLrlh6bnkIZcblx0XHRcdCQoXCIubmF2LXNsaWRlclwiKS5mYWRlT3V0KCk7XG5cdFx0XHRpZiAoJChcIiNtZW51VHJpZ2dlclwiKS5oYXNDbGFzcyhcImlzLWFjdGl2ZVwiKSkge1xuXHRcdFx0XHQkKFwiI21lbnVUcmlnZ2VyXCIpLnJlbW92ZUNsYXNzKFwiaXMtYWN0aXZlXCIpO1xuXHRcdFx0fVxuXG5cdFx0XHQvLyBUT1DjgYvjgonpm6LjgozjgovloLTlkIjjga/jg63jgrTjgajnn6LljbDjgpLpnZ7ooajnpLrjgavjgZnjgotcblx0XHRcdGlmIChpbmRleCA9PT0gMSkge1xuXHRcdFx0XHQkKCcjdG9wTG9nbycpLmNzcyh7b3BhY2l0eTogMH0pO1xuXHRcdFx0XHQkKCcjdG9wQXJyb3cnKS5jc3Moe29wYWNpdHk6IDB9KTtcblxuXHRcdFx0fVxuXHRcdH0sXG5cdFx0Ly8g44Oq44K144Kk44K65b6M44Gu44Kk44OZ44Oz44OIXG5cdFx0YWZ0ZXJSZXNpemU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdCQuZm4uZnVsbHBhZ2UucmVCdWlsZCgpO1xuXHRcdH1cblx0fSk7XG59KTtcblxuXG4vKipcbiAqIOOCsOODreODvOODkOODq+ODiuODk+OAgOOCouODs+OCq+ODvOenu+WLlVxuICogQHBhcmFtIGluZGV4IGZ1bGxwYWdlLmpzIOODquODs+OCr+OCquODluOCuOOCp+OCr+ODiFxuICovXG5mdW5jdGlvbiBuYXZBbmNob3IoaW5kZXgpIHtcblx0dmFyIHdpZHRoID0gJCgnLm5hdl9fbGlzdC1pdGVtIGEnKS5lcShpbmRleCAtMSkud2lkdGgoKTtcblx0dmFyIHBvc2l0aW9uID0gJCgnLm5hdl9fbGlzdC1pdGVtIGEnKS5lcShpbmRleCAtMSkucG9zaXRpb24oKTtcblx0JCgnI2dsb2JhbE1lbnVMaW5lJykuYW5pbWF0ZSh7XG5cdFx0d2lkdGg6IHdpZHRoLFxuXHRcdGxlZnQ6IHBvc2l0aW9uLmxlZnRcblx0fSw1MDApO1xufVxuXG5mdW5jdGlvbiBzbGlkZUluKGFuY2hvckxpbmspe1xuXHQkKCcuc2VjdGlvbicgKyBhbmNob3JMaW5rICsgJyAuc2xpZGVJbkxlZnQnKS5hbmltYXRlKHtcblx0XHR3aWR0aDogJzEwMCUnLFxuXHR9LDgwMCk7XG5cdHNldFRpbWVvdXQoIGZ1bmN0aW9uKCl7XG5cdFx0JCgnLnNlY3Rpb24nICsgYW5jaG9yTGluayArICcgLnNsaWRlSW5SaWdodCcpLmFuaW1hdGUoe1xuXHRcdFx0d2lkdGg6ICcxMDAlJyxcblx0XHR9LCA4MDApO1xuXHR9LCAxMjAwICk7XG59XG4iLCIkKGZ1bmN0aW9uKCkge1xuXHRzZXRTZWN0aW9uV2lkdGgoKTtcbn0pO1xuXG4vL+ODquOCteOCpOOCuuOBleOCjOOBn+OBqOOBjeOBruWHpueQhlxuJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbigpIHtcblx0c2V0U2VjdGlvbldpZHRoKCk7XG59KTtcblxuZnVuY3Rpb24gc2V0U2VjdGlvbldpZHRoKCl7XG5cdHZhciBzZWN0aW9uQXJyYXkgPSBbJ19fY29uY2VwdCcsICdfX25ld3MnLCAnX19hY2Nlc3MnXTtcblx0Zm9yICh2YXIgaSA9IDA7IGkgPCBzZWN0aW9uQXJyYXkubGVuZ3RoOyBpKyspIHtcblx0XHR2YXIgd2lkdGggPSAkKFwiLnNlY3Rpb25cIiArIHNlY3Rpb25BcnJheVtpXSArIFwiIC5zbGlkZUluTGVmdFdyYXBcIikud2lkdGgoKTtcblx0XHQkKFwiLnNlY3Rpb25cIiArIHNlY3Rpb25BcnJheVtpXSArIFwiIC5zbGlkZUluTGVmdElubmVyXCIpLmNzcyh7d2lkdGg6IHdpZHRofSk7XG5cblx0XHR3aWR0aCA9ICQoXCIuc2VjdGlvblwiICsgc2VjdGlvbkFycmF5W2ldICsgXCIgLnNsaWRlSW5SaWdodFdyYXBcIikud2lkdGgoKTtcblx0XHQkKFwiLnNlY3Rpb25cIiArIHNlY3Rpb25BcnJheVtpXSArIFwiIC5zbGlkZUluUmlnaHRJbm5lclwiKS5jc3Moe3dpZHRoOiB3aWR0aH0pO1xuXHR9XG5cblx0dmFyIHNjcmVlbldpZHRoID0gJCgnYm9keScpLndpZHRoKCk7XG5cblx0aWYgKHNjcmVlbldpZHRoIDwgOTgwKSB7XG5cdFx0dmFyIHNlY3Rpb25BcnJheSA9IFsnX19nYWxsZXJ5JywgJ19fc3R5bGUnXTtcblxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgc2VjdGlvbkFycmF5Lmxlbmd0aDsgaSsrKSB7XG5cdFx0XHR2YXIgd2lkdGggPSAkKFwiLnNlY3Rpb25cIiArIHNlY3Rpb25BcnJheVtpXSArIFwiIC5zbGlkZUluTGVmdFdyYXBcIikud2lkdGgoKTtcblx0XHRcdCQoXCIuc2VjdGlvblwiICsgc2VjdGlvbkFycmF5W2ldICsgXCIgLnNsaWRlSW5MZWZ0SW5uZXJcIikuY3NzKHt3aWR0aDogd2lkdGh9KTtcblxuXHRcdFx0d2lkdGggPSAkKFwiLnNlY3Rpb25cIiArIHNlY3Rpb25BcnJheVtpXSArIFwiIC5zbGlkZUluUmlnaHRXcmFwXCIpLndpZHRoKCk7XG5cdFx0XHQkKFwiLnNlY3Rpb25cIiArIHNlY3Rpb25BcnJheVtpXSArIFwiIC5zbGlkZUluUmlnaHRJbm5lclwiKS5jc3Moe3dpZHRoOiB3aWR0aH0pO1xuXHRcdH1cblxuXHR9IGVsc2Uge1xuXHRcdCQoXCIuc2VjdGlvbl9fZ2FsbGVyeSAuc2xpZGVJbkxlZnRJbm5lclwiKS5jc3Moe3dpZHRoOiA0MjB9KTtcblx0XHR2YXIgd2lkdGggPSAkKCcjc2xpZGVyR2FsbGVyeScpLndpZHRoKCk7XG5cblx0XHQkKFwiLnNlY3Rpb25fX3N0eWxlIC5zbGlkZUluTGVmdElubmVyXCIpLmNzcyh7d2lkdGg6IDQyMH0pO1xuXHRcdHZhciB3aWR0aCA9ICQoJyNzbGlkZXJHYWxsZXJ5Jykud2lkdGgoKTtcblx0fVxufVxuIiwiJChmdW5jdGlvbigpIHtcblx0dmFyIGJvZHlDbGFzcyA9ICQoXCJib2R5XCIpLmF0dHIoJ2lkJyk7XG5cdGlmIChib2R5Q2xhc3MgIT09ICdob21lJykgcmV0dXJuIGZhbHNlO1xuXHR2YXIgaW1hZ2VzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2ltZycpOyAvLyDjg5rjg7zjgrjlhoXjga7nlLvlg4/lj5blvpdcblx0dmFyIHBlcmNlbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbG9hZE51bScpOyAvLyDjg5Hjg7zjgrvjg7Pjg4jjga7jg4bjgq3jgrnjg4jpg6jliIZcblx0dmFyIGxvYWRpbmdCZyA9ICQoJyNsb2FkJyk7IC8vIOODreODvOODh+OCo+ODs+OCsOiDjOaZr1xuXHR2YXIgaW1nQ291bnQgPSAwO1xuXHR2YXIgYmFzZUNvdW50ID0gMDtcblx0dmFyIGN1cnJlbnQ7XG5cblx0Ly8g55S75YOP44Gu6Kqt44G/6L6844G/XG5cdGZvciAodmFyIGkgPSAwOyBpIDwgaW1hZ2VzLmxlbmd0aDsgaSsrKSB7XG5cdFx0dmFyIGltZyA9IG5ldyBJbWFnZSgpOyAvLyDnlLvlg4/jga7kvZzmiJBcblx0XHQvLyDnlLvlg4/oqq3jgb/ovrzjgb/lrozkuobjgZfjgZ/jgajjgY1cblx0XHRpbWcub25sb2FkID0gZnVuY3Rpb24oKSB7XG5cdFx0XHRpbWdDb3VudCArPSAxO1xuXHRcdH1cblx0XHQvLyDnlLvlg4/oqq3jgb/ovrzjgb/lpLHmlZfjgZfjgZ/jgajjgY1cblx0XHRpbWcub25lcnJvciA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0aW1nQ291bnQgKz0gMTtcblx0XHR9XG5cdFx0aW1nLnNyYyA9IGltYWdlc1tpXS5zcmM7IC8vIOeUu+WDj+OBq3NyY+OCkuaMh+WumuOBl+OBpuiqreOBv+i+vOOBv+mWi+Wni1xuXHR9O1xuXG5cdC8vIOODreODvOODh+OCo+ODs+OCsOWHpueQhlxuXHR2YXIgbm93TG9hZGluZyA9IHNldEludGVydmFsKGZ1bmN0aW9uKCkge1xuXHRcdGlmKGJhc2VDb3VudCA8PSBpbWdDb3VudCkgeyAvLyBiYXNlQ291bnTjgYxpbWdDb3VudOOCkui/veOBhOaKnOOBi+OBquOBhOOCiOOBhuOBq+OBmeOCi1xuXHRcdFx0Ly8g54++5Zyo44Gu6Kqt44G/6L6844G/5YW35ZCI44Gu44OR44O844K744Oz44OI44KS5Y+W5b6XXG5cdFx0XHRjdXJyZW50ID0gTWF0aC5mbG9vcihiYXNlQ291bnQgLyBpbWFnZXMubGVuZ3RoICogMTAwKTtcblx0XHRcdC8vIOODkeODvOOCu+ODs+ODiOihqOekuuOBruabuOOBjeaPm+OBiFxuXHRcdFx0cGVyY2VudC5pbm5lckhUTUwgPSBjdXJyZW50O1xuXHRcdFx0YmFzZUNvdW50ICs9IDE7XG5cdFx0XHQvLyDlhajjgaboqq3jgb/ovrzjgpPjgaDmmYJcblx0XHRcdC8vIGlmKGJhc2VDb3VudCA9PSBpbWFnZXMubGVuZ3RoKSB7XG5cdFx0XHRpZihjdXJyZW50ID09PSAxMDApIHtcblx0XHRcdFx0Ly8g44Ot44O844OH44Kj44Oz44Kw6KaB57Sg44Gu6Z2e6KGo56S6XG5cdFx0XHRcdHNldFRpbWVvdXQoIGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0bG9hZGluZ0JnLmNzcyh7b3BhY2l0eTogMH0pO1xuXHRcdFx0XHR9LCA1MDAgKTtcblx0XHRcdFx0c2V0VGltZW91dCggZnVuY3Rpb24oKXtcblx0XHRcdFx0XHRsb2FkaW5nQmcuY3NzKHtkaXNwbGF5OiBcIm5vbmVcIn0pO1xuXHRcdFx0XHR9LCAxMDAwICk7XG5cdFx0XHRcdC8vIG5hduOBruihqOekulxuXHRcdFx0XHRzZXRUaW1lb3V0KCBmdW5jdGlvbigpe1xuXHRcdFx0XHRcdCQoXCIjbmF2VG9wLCAjaGVhZGVyVG9wXCIpLmFuaW1hdGUoe1xuXHRcdFx0XHRcdFx0b3BhY2l0eTogXCIxXCIsXG5cdFx0XHRcdFx0XHRtYXJnaW5Ub3A6IFwiMFwiXG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH0sIDI1MDAgKTtcblx0XHRcdFx0Ly8gc25z44Gu6KGo56S6XG5cdFx0XHRcdHNldFRpbWVvdXQoIGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0JChcIiNzbnNUb3BcIikuYW5pbWF0ZSh7XG5cdFx0XHRcdFx0XHRvcGFjaXR5OiBcIjFcIixcblx0XHRcdFx0XHRcdGxlZnQ6IFwiMjRweFwiXG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH0sIDMwMDAgKTtcblx0XHRcdFx0Ly8g44Ot44O844OH44Kj44Oz44Kw44Gu57WC5LqGXG5cdFx0XHRcdGNsZWFySW50ZXJ2YWwobm93TG9hZGluZyk7XG5cdFx0XHR9XG5cblx0XHR9XG5cdH0sIDUpO1xufSk7XG4iLCJ2YXIgaW5pdCA9IHJlcXVpcmUoICcuL2luaXQuanMnICk7XG52YXIgbG9hZGluZyA9IHJlcXVpcmUoICcuL2xvYWRpbmcuanMnICk7XG52YXIgZnVsbHBhZ2UgPSByZXF1aXJlKCAnLi9mdWxscGFnZS5qcycgKTtcbnZhciBzbGlkZXIgPSByZXF1aXJlKCAnLi9zbGlkZXIuanMnICk7XG52YXIgbmF2U2xpZGVyID0gcmVxdWlyZSggJy4vbmF2LXNsaWRlci5qcycgKTtcbnZhciBzcE1vZGFsID0gcmVxdWlyZSggJy4vc3AtbW9kYWwuanMnICk7XG52YXIgYXJyb3cgPSByZXF1aXJlKCAnLi9hcnJvdy5qcycgKTtcbnZhciBuYXZBbmNob3IgPSByZXF1aXJlKCAnLi9uYXYtYW5jaG9yLmpzJyApO1xudmFyIG1hc29ucnkgPSByZXF1aXJlKCAnLi9tYXNvbnJ5LmpzJyApO1xuIiwiLy8gJChmdW5jdGlvbigpIHtcbiQod2luZG93KS5vbignbG9hZCcsIGZ1bmN0aW9uKCl7XG5cdCQoJy5tYXNvbnJ5JykubWFzb25yeSh7XG5cdFx0Ly8gb3B0aW9uc1xuXHRcdGl0ZW1TZWxlY3RvcjogJy5tYXNvbnJ5X19pdGVtJyxcblx0XHRwZXJjZW50UG9zaXRpb246IHRydWVcblx0fSk7XG59KTtcbiIsIiQoZnVuY3Rpb24oKSB7XG5cdGlmICgkKFwiYm9keVwiKS5hdHRyKFwiaWRcIikgPT09IFwiaG9tZVwiKSByZXR1cm4gZmFsc2U7XG5cdHZhciB3aWR0aCA9ICQoJyNnbG9iYWxNZW51IGEuaXMtYWN0aXZlJykud2lkdGgoKTtcblx0dmFyIHBvc2l0aW9uID0gJCgnI2dsb2JhbE1lbnUgYS5pcy1hY3RpdmUnKS5wb3NpdGlvbigpO1xuXHQkKCcjZ2xvYmFsTWVudUxpbmUnKS5jc3Moe1xuXHRcdHdpZHRoOiB3aWR0aCxcblx0XHRsZWZ0OiBwb3NpdGlvbi5sZWZ0XG5cdH0sNTAwKTtcbn0pO1xuIiwiJChmdW5jdGlvbigpIHtcblx0dmFyIGJvZHlJZCA9ICQoXCJib2R5XCIpLmF0dHIoXCJpZFwiKTtcblx0JChcIiNtZW51VHJpZ2dlclwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG5cdFx0aWYgKCQodGhpcykuaGFzQ2xhc3MoXCJpcy1hY3RpdmVcIikpIHtcblx0XHRcdCQoXCIubmF2LXNsaWRlclwiKS5mYWRlT3V0KCk7XG5cdFx0XHQvLyBmdWxscGFnZS5qc+OBruOCueOCr+ODreODvOODq+OCkuacieWKueWMluOBmeOCi1xuXHRcdFx0aWYgKGJvZHlJZCA9PT0gXCJob21lXCIpIHtcblx0XHRcdFx0JC5mbi5mdWxscGFnZS5zZXRBbGxvd1Njcm9sbGluZyh0cnVlKTtcblx0XHRcdH1cblx0XHR9IGVsc2Uge1xuXHRcdFx0JChcIi5uYXYtc2xpZGVyXCIpLmZhZGVJbigpO1xuXHRcdFx0Ly8gZnVsbHBhZ2UuanPjga7jgrnjgq/jg63jg7zjg6vjgpLnhKHlirnljJbjgZnjgotcblx0XHRcdGlmIChib2R5SWQgPT09IFwiaG9tZVwiKSB7XG5cdFx0XHRcdCQuZm4uZnVsbHBhZ2Uuc2V0QWxsb3dTY3JvbGxpbmcoZmFsc2UpO1xuXHRcdFx0fVxuXHRcdH1cblx0XHQkKHRoaXMpLnRvZ2dsZUNsYXNzKFwiaXMtYWN0aXZlXCIpO1xuXHR9KTtcblxuXHQkKFwiI25hdlNsaWRlckxpc3QgYVwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG5cdFx0aWYgKGJvZHlJZCA9PT0gXCJob21lXCIpIHtcblx0XHRcdCQuZm4uZnVsbHBhZ2Uuc2V0QWxsb3dTY3JvbGxpbmcodHJ1ZSk7XG5cdFx0fVxuXHR9KTtcbn0pO1xuIiwidmFyIHN3aXBlckdhbGxlcnkgPSBuZXcgU3dpcGVyKCcjc2xpZGVyR2FsbGVyeScsIHtcblx0c2xpZGVzUGVyVmlldzogMixcblx0Ly8gY2VudGVyZWRTbGlkZXM6IHRydWUsXG5cdHNwYWNlQmV0d2VlbjogMzAsXG5cdGhlaWdodDogNDcwXG59KTtcblxudmFyIHN3aXBlclN0eWxlID0gbmV3IFN3aXBlcignI3NsaWRlclN0eWxlJywge1xuXHRzbGlkZXNQZXJWaWV3OiAzLjMsXG5cdC8vIGNlbnRlcmVkU2xpZGVzOiB0cnVlLFxuXHRzcGFjZUJldHdlZW46IDMwLFxuXHRoZWlnaHQ6IDQ3MFxufSk7XG4kKCcjc2xpZGVyTmV4dEdhbGxlcnknKS5jbGljayhmdW5jdGlvbigpIHtcblx0c3dpcGVyR2FsbGVyeS5zbGlkZU5leHQoNTAwKTtcbn0pO1xuXG4kKCcjc2xpZGVyTmV4dFN0eWxlJykuY2xpY2soZnVuY3Rpb24oKSB7XG5cdHN3aXBlclN0eWxlLnNsaWRlTmV4dCg1MDApO1xufSk7XG4iLCIkKGZ1bmN0aW9uKCkge1xuXHQkKFwiYm9keVwiKS5vbihcImNsaWNrXCIsIFwiI29wZW5Nb2RhbENvbmNlcHRcIiwgZnVuY3Rpb24gKCkge1xuXHRcdCQoXCIjY29uY2VwdE1vZGFsXCIpLmZhZGVJbigpO1xuXHRcdC8vIGZ1bGxwYWdlLmpz44Gu44K544Kv44Ot44O844Or44KS54Sh5Yq55YyW44GZ44KLXG5cdFx0JC5mbi5mdWxscGFnZS5zZXRBbGxvd1Njcm9sbGluZyhmYWxzZSk7XG5cdH0pO1xuXG5cdCQoXCJib2R5XCIpLm9uKFwiY2xpY2tcIiwgXCIjb3Blbk1vZGFsQWNjZXNzXCIsIGZ1bmN0aW9uICgpIHtcblx0XHQkKFwiI2FjY2Vzc01vZGFsXCIpLmZhZGVJbigpO1xuXHRcdC8vIGZ1bGxwYWdlLmpz44Gu44K544Kv44Ot44O844Or44KS54Sh5Yq55YyW44GZ44KLXG5cdFx0JC5mbi5mdWxscGFnZS5zZXRBbGxvd1Njcm9sbGluZyhmYWxzZSk7XG5cdH0pO1xuXG5cdCQoXCJib2R5XCIpLm9uKFwiY2xpY2tcIiwgXCIuc3BNb2RhbENsb3NlXCIsIGZ1bmN0aW9uICgpIHtcblx0XHQkKFwiLnNwLW1vZGFsXCIpLmZhZGVPdXQoKTtcblx0XHQvLyBmdWxscGFnZS5qc+OBruOCueOCr+ODreODvOODq+OCkuacieWKueWMluOBmeOCi1xuXHRcdCQuZm4uZnVsbHBhZ2Uuc2V0QWxsb3dTY3JvbGxpbmcodHJ1ZSk7XG5cdH0pO1xufSk7XG4iXX0=
