<?php
$paged = (int) get_query_var('paged');
$arg = array(
    'posts_per_page' => 9, // 表示する件数
    'paged' => $paged,
    'orderby' => 'date', // 日付でソート
    'order' => 'DESC', // DESCで最新から表示、ASCで最古から表示
    'category_name' => 'style', // 表示したいカテゴリーのスラッグを指定
    'post_status' => 'publish'
);
$posts = get_posts( $arg );
$post_num =  count($posts);
?>
<main class="main">
    <div class="content-header content-header--style">
        <div class="content-header__title">
            <h2 class="content-header__title--en">Style</h2>
            <p class="content-header__title--ja">スタイル</p>
        </div>
    </div>
    <div class="container">
        <?php breadcrumb(); ?>
        <div class="container__inner">
            <?php if( $posts ): ?>
                <ul class="list-style">
                    <?php foreach ( $posts as $post ) : ?>
                        <?php setup_postdata( $post ); ?>
                        <?php $imageId = get_post_thumbnail_id();?>
                        <?php $imageUrl = wp_get_attachment_image_src($imageId, true); ?>
                        <?php $thumUrl = (is_array($imageUrl) && $imageUrl[0] !== '') ? $imageUrl[0] : get_template_directory_uri() . '/images/img_dummy.png'; ?>
                        <li class="list-style__item">
                            <article class="list-style__article"><a class="list-style__link" href="<?php the_permalink(); ?>">
                                    <div class="list-style__thumb">
                                        <img class="list-style__image" src="<?php echo $thumUrl; ?>"/>
                                    </div>
                                    <div class="list-style__contents">
                                        <header>
                                            <div class="list-style__date"><?php the_time( 'Y.m.d' ); ?></div>
                                            <h3 class="list-style__title"><?php the_title(); ?></h3>
                                        </header>
                                        <div class="list-style__text"><?php the_excerpt(); ?></div>
                                    </div>
                                </a>
                            </article>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; wp_reset_postdata();?>
            <?php pagination($wp_query->max_num_pages, get_query_var( 'paged' )); ?>
        </div>
    </div>
</main>
