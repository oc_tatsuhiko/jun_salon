<main class="main">
    <div class="content-header content-header--gallery">
        <div class="content-header__title">
            <h2 class="content-header__title--en">Gallery</h2>
            <p class="content-header__title--ja">ギャラリー</p>
        </div>
    </div>
    <div class="container">
        <?php breadcrumb(); ?>
        <div class="container__inner">
            <ul class="masonry">
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery01.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery11.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery02.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery03.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery21.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery22.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery06.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery23.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery04.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery07.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery17.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery05.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery09.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery12.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery08.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery24.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery10.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery13.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery16.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery15.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery14.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery19.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery18.jpg"/></li>
                <li class="masonry__item"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery/img_gallery20.jpg"/></li>
            </ul>
        </div>
    </div>
</main>
