<?php
/**
 * The template for displaying the head
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <?php if($_SERVER['REQUEST_URI'] === '/news'): ?>
        <meta name="description" content="名前のない美容室のニュースページです。最新の休業情報やイベント情報を掲載しています。">
    <?php elseif($_SERVER['REQUEST_URI'] === '/style'): ?>
        <meta name="description" content="名前のない美容室のスタイルブログページです。新メニューのご案内やおすすめ商品の紹介、撮影の事など様々な事を発信しています。">
    <?php endif; ?>
    <?php if( is_single() ): ?>
        <?php if( have_posts() ): ?>
            <?php while( have_posts() ): the_post(); ?>
                <link rel="alternate" hreflang="ja" href="<?php the_permalink(); ?>">
            <?php endwhile; ?>
        <?php endif; ?>
    <?php elseif( is_home() ): ?>
        <link rel="alternate" hreflang="ja" href="<?php echo home_url(); ?>">
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.fullPage.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/swiper.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css?v=<?php echo date('YmdHis'); ?>">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
    <?php if ($_SERVER['HTTP_HOST'] == 'jun.salon'): ?>
        <!-- Google Tag Manager-->
        <script>
            (function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-P9L5ZJ2');
        </script>
        <!-- End Google Tag Manager-->
    <?php endif;  ?>
    <?php wp_head(); ?>
</head>

<body <?php if ( is_home() || is_front_page() ) : ?>id="home"<?php endif; ?>>
    <?php if ($_SERVER['HTTP_HOST'] == 'jun.salon'): ?>
    <!-- Google Tag Manager (noscript)-->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9L5ZJ2" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript)-->
    <?php endif;  ?>
