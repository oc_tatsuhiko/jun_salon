<?php
/**
 * The template for displaying the script
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */

?>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/masonry.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/scrolloverflow.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.fullPage.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/swiper.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/bundle.js"></script>
<?php wp_footer(); ?>
</body>
</html>
