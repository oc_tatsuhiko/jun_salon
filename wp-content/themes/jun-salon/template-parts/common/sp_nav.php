<?php
/**
 * The template for displaying the sp nav
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */

?>
<?php
if ( is_home() || is_front_page() ) {
    $home_flg = true;
    $single_flg = false;
    $url = [
        'top' => '#__top',
        'concept' => '#__concept',
        'gallery' => '#__gallery',
        'style' => '#__style',
        'news' => '#__news',
        'access' => '#__access',
    ];
} elseif($_SERVER['REQUEST_URI'] === '/gallery' || $_SERVER['REQUEST_URI'] === '/style') {
    $home_flg = false;
    $single_flg = false;
    $url = [
        'top' => '/',
        'concept' => '/#__concept',
        'gallery' => '/gallery',
        'style' => '/style',
        'news' => '/news',
        'access' => '/#__access',
    ];
} else {
    $home_flg = false;
    $single_flg = true;
    $url = [
        'top' => '/',
        'concept' => '/#__concept',
        'gallery' => '/gallery',
        'style' => '/style',
        'news' => '/news',
        'access' => '/#__access',
    ];
}
?>
<div class="button-menu <?php if($single_flg): ?>button-menu--single<?php endif; ?>">
    <span class="button-menu__trigger" id="menuTrigger" href="javascript:void(0);">
        <span></span>
        <span></span>
        <span></span>
    </span>
</div>
<div class="nav-slider" id="navSlider">
    <div class="nav-slider__wrap">
        <ul class="nav-slider__list" id="navSliderList">
            <li class="nav-slider__list-item" <?php if($home_flg): ?>data-menuanchor="__top"<?php endif; ?>>
                <a class="nav-slider__link" href="<?php echo $url['top']; ?>">ホーム</a>
            </li>
            <li class="nav-slider__list-item" <?php if($home_flg): ?>data-menuanchor="__concept"<?php endif; ?>>
                <a class="nav-slider__link" href="<?php echo $url['concept']; ?>">コンセプト</a>
            </li>
            <li class="nav-slider__list-item" <?php if($home_flg): ?>data-menuanchor="__gallery"<?php endif; ?>>
                <a class="nav-slider__link" href="<?php echo $url['gallery']; ?>">ギャラリー</a>
            </li>
            <li class="nav-slider__list-item" <?php if($home_flg): ?>data-menuanchor="__style"<?php endif; ?>>
                <a class="nav-slider__link" href="<?php echo $url['style']; ?>">スタイル</a>
            </li>
            <li class="nav-slider__list-item" <?php if($home_flg): ?>data-menuanchor="__news"<?php endif; ?>>
                <a class="nav-slider__link" href="<?php echo $url['news']; ?>">ニュース</a>
            </li>
            <li class="nav-slider__list-item" <?php if($home_flg): ?>data-menuanchor="__access"<?php endif; ?>>
                <a class="nav-slider__link" href="<?php echo $url['access']; ?>">アクセス</a>
            </li>
            <li class="nav-slider__list-item">
                <a class="nav-slider__link" href="https://work.salonboard.com/slnH000431776/" target="_blank">リクルート</a>
            </li>
        </ul>
        <div class="nav-footer">
            <ul class="nav-footer__list">
                <li class="nav-footer__list-item">
                    <a class="nav-footer__link" href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3487057&pid=886135206&vc_url=https%3A%2F%2Fbeauty.hotpepper.jp%2FslnH000431776%2Fcoupon%2F" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3487057&pid=886135206" height="1" width="0" border="0">予約する</a>
                </li>
                <li class="nav-footer__list-item">
                    <a class="nav-footer__link" href="/contact">お問い合わせ</a>
                </li>
            </ul>
            <ul class="nav-footer__sns-list">
                <li class="nav-footer__sns-list-item">
                    <a class="nav-footer__link" href="https://www.instagram.com/junsan0402/" target="_blank">
                        <img class="nav-footer__icon" src="<?php echo get_template_directory_uri(); ?>/images/icon_instagram.png">
                    </a>
                </li>
                <li class="nav-footer__sns-list-item">
                    <a class="nav-footer__link" href="https://line.me/R/ti/p/%40bdf0572g">
                        <img class="nav-footer__icon" src="<?php echo get_template_directory_uri(); ?>/images/icon_line.png">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
