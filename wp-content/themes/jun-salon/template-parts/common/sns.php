<?php
/**
 * The template for displaying the home sns
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */

?>

<?php
    $sns_class      = ( is_home() || is_front_page() ) ? 'list-sns--top' : 'list-sns';
    $img_instagram  = ( is_home() || is_front_page() ) ? 'icon_instagram' : 'icon_instagram_dark';
    $img_line       = ( is_home() || is_front_page() ) ? 'icon_line' : 'icon_line_dark';
?>

<ul class="<?php echo $sns_class; ?> u-disp--pc" <?php if ( is_home() || is_front_page() ) : ?> id="snsTop" <?php endif; ?>>
    <li class="list-sns__item">
        <a class="list-sns__link" href="https://www.instagram.com/junsan0402/" target="_blank">
            <img class="list-sns__icon" src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $img_instagram; ?>.png">
        </a>
        <span class="list-sns__balloon">Instagram</span>
    </li>
    <li class="list-sns__item">
        <a class="list-sns__link" href="https://line.me/R/ti/p/%40bdf0572g">
            <img class="list-sns__icon" src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $img_line; ?>.png">
        </a>
        <span class="list-sns__balloon">Line@</span>
    </li>
</ul>
