<?php
/**
 * The template for displaying the home loading
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */

?>
<div class="load" id="load">
    <div class="load__wrap"><img class="load__logo" src="<?php echo get_template_directory_uri(); ?>/images/img_logo.jpg" alt=""><span class="load__num"><span id="loadNum">100</span>%</span></div>
</div>
