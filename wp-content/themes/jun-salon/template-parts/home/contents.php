<?php
/**
 * The template for displaying the home contents
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */

?>
<div id="fullpage">
    <div class="section section__top">
        <svg class="section__top-logo" id="topLogo" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 717.6 73.03">
            <title>名前のない美容室</title>
            <g id="logo" data-name="logo">
                <g id="logo" data-name="logo" style="fill: #fff;">
                    <path d="M57.22,42.54c1.28-1.76,3.12-3.84,4.08-3.84,1.76,0,6.8,3.76,6.8,5.6,0,1.28-1.68,2-3.36,2.64-.16,6.32.08,18.8.16,21.84,0,2.48-1.36,3.12-3.6,3.12-1.92,0-2.48-1.44-2.48-2.72v-3.6H29.38v4.24c0,1.76-1.28,2.56-3.68,2.56a2,2,0,0,1-2.24-2.24c.08-4.24.32-14.72.16-22.8a137.62,137.62,0,0,1-18,7.36c-2.24.88-3.36-1.84-1.12-2.8a129.92,129.92,0,0,0,18.88-9.28,15.73,15.73,0,0,0-.16-2.08,1,1,0,0,1,1.12-1,15.49,15.49,0,0,1,2.72.72C39,32.14,45.78,23.34,50,14.14H27c-5.84,9.2-13.92,19.52-24.32,26.8C.66,42.38-1,40.62.74,38.86c11.52-11,20.72-24.08,25.52-37.44.32-.8.64-1.12,1.52-1,2.8.56,7.76,2.72,7.76,4.32,0,1.2-1.6,1.68-3.84,1.44-.8,1.52-1.76,3.12-2.72,4.72H49.54C51,9.1,53.06,6.86,54,6.86c1.6,0,6.48,5,6.48,6.88,0,1-2.08,1.84-4.08,2.08A71,71,0,0,1,31.78,42.54ZM26.74,29.66a52,52,0,0,0-3.92-5.2c-1.12-1.36-.56-3,1.6-2.56,5,1,10,4,10.4,8.08a3.41,3.41,0,0,1-3.2,4.08C29.46,34.3,28.42,32.3,26.74,29.66ZM58.9,62.38c0-5.52.08-13,.08-16.64H29.46C29.3,48,29.3,55,29.3,56.62v5.76Z"></path>
                    <path d="M156.3,14.7c1.76-2.64,4.16-5.76,5.44-5.76,1.6,0,7.76,5,7.76,7,0,1.12-1.44,1.92-4.64,1.92h-68c-1,0-1.76-.32-1.76-1.6s.72-1.6,1.76-1.6H138a2,2,0,0,1,.16-1.76A36,36,0,0,0,143.5,1.1c.08-.72.56-1.28,1.36-1,3,.64,7.76,3.2,7.84,4.56s-1.36,1.76-3.76,1.68a30.77,30.77,0,0,1-8.4,8.32l-.08.08ZM121.5,25.9c1.2-1.52,2.8-3.28,3.36-3.28,1.84,0,6.72,4.08,6.72,5.52,0,1-1.36,1.84-3,2.4-.24,8.72-.08,23.2-.16,34.88,0,4.88-3.12,6.88-6.56,6.88-3.28,0-1.68-3-5.44-4.48-1.6-.8-1.2-2.56.32-2.48.8.08,2.72.16,4.08.16,1.52,0,2.16-.72,2.32-2.8s.16-5.44.16-8.8h-14.8c0,6.16.08,14.64.08,16.32s-1.2,2.64-3.68,2.64a2,2,0,0,1-2.16-2.24c0-7.68.8-37.12-.32-46.8-.08-.64.48-1.12,1.2-1.12A21,21,0,0,1,111,25.9Zm1.84,24.8V41.58H108.46V50.7ZM108.78,29.1c-.08,1.84-.16,5.6-.24,9.28h14.8V29.1Zm7.68-24.64c-1-1.68,0-3.44,2.32-2.48,4.24,1.68,8.32,4.4,8.16,8.16a3.77,3.77,0,0,1-4.08,3.68c-2.16-.16-2.8-2.24-3.84-4.64C118.3,7.66,117.34,6,116.46,4.46ZM145.1,43.82c0,2.64.16,13.52.16,15.44,0,1.76-1.44,2.32-3.84,2.32a2,2,0,0,1-2.32-2.08c0-2,.48-12.8.48-15.76,0-2.08.08-12.64-.64-17-.16-.8.32-1.12,1-1.12,3.2,0,9,.24,9,2.24,0,1.2-1.36,2.08-3.84,2.48Zm20-18.88c0,1.2-1.76,1.84-3.6,2.16-.08,8.32,0,25.76,0,39.2,0,4-3,6.4-6.88,6.4-1.52,0-2.08-.88-3.44-2.16s-2.32-1.6-4.56-2.24c-1.76-.48-1.52-2.48.08-2.4s3.44.24,5.12.24c3,0,3.76-1,3.92-3.12.32-4.16.16-11.68.16-17.68,0-4.48-.32-16-.8-21.52-.08-1,.16-1.36,1.2-1.36C160.54,22.46,165.1,23.34,165.1,24.94Z"></path>
                    <path d="M249.44,44c0,13.82-11.09,20.95-25.92,20.95-2.52,0-2.67-2.23-.43-2.66,12.45-2.45,20.52-7.71,20.52-19.51,0-7.71-3.89-14.12-11.16-17.14a34.9,34.9,0,0,1-2,8.85c-4.17,11.53-16.2,25.86-24.84,25.86-3.82,0-7.56-5.26-8.21-13.76-.72-9.07,2.81-15.77,7.56-20a27.45,27.45,0,0,1,17.64-6.4C239.79,20.13,249.44,31,249.44,44Zm-21-19.66a32,32,0,0,0-6-.57c-6.2,0-11.09,2-14.62,5.32-3.82,3.61-6.12,8.72-5.69,15.85.36,5.54,2.3,8.93,4.39,8.93,6.92,0,17.28-14,20.67-23.7A22.69,22.69,0,0,0,228.42,24.3Z"></path>
                    <path d="M332.52,64.55c0,.8-.15,2.45-2.17,2.45s-3.16-1.37-5.18-3.38c-2.74-2.74-5.26-5.11-9.43-6.27a21.31,21.31,0,0,1,.86,5.33c0,3-2.23,6.55-8.35,6.55s-9.94-2.59-9.94-7.34c0-3.74,2.95-8.21,12.46-8.5a27.39,27.39,0,0,1-.36-4.61c0-7.05,5.47-10.36,11.59-11.88-2.16-1.65-4.75-2.3-8.49-2.3-8.5,0-18.22,9-24.27,18.29-2.59,4-5.33,9-8.28,9-1.37,0-2.16-1.3-2.16-2.74,0-2.45,2.59-4.89,6.19-9.57a98.25,98.25,0,0,0,12.82-21.75,72.71,72.71,0,0,1-9.58.72c-4.54,0-8-4.1-8-8.57a1,1,0,0,1,1.73-.79c1.36,1.08,4.82,5.18,10.08,5.18a48.76,48.76,0,0,0,7.2-.64c.79-2.38,1.29-4.25,1.51-5.19.36-1.58-.22-2.09-2.09-2.88-1.51-.65-3.24-1.08-3.24-2.16s2.45-1.73,4.25-1.73c3.53,0,6.55,1.44,6.55,3.68,0,1.08-1,4.1-2.23,7.41a53.91,53.91,0,0,0,9.94-3.45,13.74,13.74,0,0,1,5.47-1.73c1.94,0,3.74,1.08,3.74,2.3s-1,1.8-2.73,2.59S309.11,26,302.27,27.25c-1.51,3.39-3.74,8.14-6.26,13,6.91-5.91,12.67-8.64,19.51-8.64,9,0,16.28,5.33,16.28,9.57a1.84,1.84,0,0,1-1.88,1.8,4.87,4.87,0,0,1-3.38-1.72c-1.15-1-2.38-1.44-5.19-1.16-5.18.51-7.34,3.68-7.34,7.71a26.53,26.53,0,0,0,.79,5.76c4.32.57,10.23,2.3,14.19,5.54C330.93,60.74,332.52,62.9,332.52,64.55Zm-21.1-7.85a11.57,11.57,0,0,0-1.59-.07c-2.95,0-8.21,1.3-8.21,5,0,1.8,1.16,3,4.54,3,4.39,0,5.69-2.16,5.69-4.61A13.1,13.1,0,0,0,311.42,56.7Z"></path>
                    <path d="M415.46,49.29c0,2.88-1.37,4.68-3.6,4.68-2.52,0-2.59-4.61-3.39-8.43-1.22-5.83-4.24-10.8-8.42-10.8-5.33,0-11.16,13.83-15.34,21.39-2.59,4.68-3.74,6.26-6.91,6.26-6.05,0-12.31-7.34-12.31-15.62,0-5.76.79-8,.79-11.24,0-3.81-1-5-4.32-8.78-1-1.15-.22-2.16,1.29-1.87,4.69.86,9.15,5.18,9.15,8.64,0,3.67-2.59,8.06-2.59,12.82,0,6.62,4.46,9.72,7.34,9.72,2,0,4.11-3.39,6.27-6.7,6.77-10.37,11.16-18.15,17.06-18.15a12,12,0,0,1,8.93,4.18C413,39.14,415.46,44.61,415.46,49.29Z"></path>
                    <path d="M486,52.94c5.52,9.28,16,12.24,30.24,12.72,2.56.16,2.48,1.84.48,2.56a7.34,7.34,0,0,0-3.68,2.56c-1,1.52-1.6,2.16-3.92,1.68-13.12-1.92-22-7.68-26.48-19.52h-1.84a28,28,0,0,1-6.64,10.48c-5.76,5.36-15.76,9-28.32,9.6-2.48.16-2.4-2.32-.16-2.72,10.64-1.84,19.36-5,24.08-10.16a24.65,24.65,0,0,0,4.64-7.2H446.33c-1,0-1.76-.32-1.76-1.6s.72-1.6,1.76-1.6h29.2a40.25,40.25,0,0,0,1.52-8.32,3.19,3.19,0,0,1,.32-1H447.45c-1,0-1.76-.32-1.76-1.6s.72-1.6,1.76-1.6h30.24V28.62H456c-1,0-1.76-.32-1.76-1.6s.72-1.6,1.76-1.6h21.68V17.5H451.21c-1,0-1.76-.32-1.76-1.6s.72-1.6,1.76-1.6h35c-.64-.4-.8-1.2-.24-2.24a35.68,35.68,0,0,0,5-10.88,1.33,1.33,0,0,1,1.76-1c2.8,1,7.36,3.84,7.2,5.36-.08,1.2-1.44,1.52-3.92,1.28A27.71,27.71,0,0,1,488.41,14a4.59,4.59,0,0,1-.56.32h13.76c1.6-2.4,3.68-5,4.8-5,1.6,0,7.12,4.56,7.2,6.48,0,1.12-1.28,1.76-4.32,1.76H484.41a1.11,1.11,0,0,1,.4.72c0,.72-.48,1.28-1.36,1.68v5.52h13.2c1.52-2.24,3.44-4.8,4.56-4.8,1.52,0,6.88,4.48,6.88,6.32.08,1.12-1.2,1.68-4.08,1.68H483.45v8.56h19.84c1.68-2.4,3.84-5.28,5-5.28,1.6,0,7.28,4.72,7.28,6.72,0,1.12-1.2,1.76-4.32,1.76H481.53c2.72.64,5.36,1.84,5.36,3,0,1-1.36,1.68-4,1.92-.32,1.52-.64,3-1,4.4h21.92c1.84-2.64,4.16-5.76,5.44-5.76,1.68,0,7.76,5,7.76,7,0,1.12-1.36,1.92-4.64,1.92ZM464.09,3.5c-.88-1.6,0-3.12,2.16-2.32,4.24,1.6,8.48,4.64,8.24,8.48a3.54,3.54,0,0,1-4,3.52c-2.08-.16-2.88-2.16-3.84-4.64A33.18,33.18,0,0,0,464.09,3.5Z"></path>
                    <path d="M570,70.38c0,1.76-1.2,2.48-3.52,2.48a1.91,1.91,0,0,1-2.16-2.16c0-4.8.32-16.4-.16-23.76a65.07,65.07,0,0,1-15.92,7c-2.16.64-3-1.92-.8-2.88,12.64-5.36,26.4-15.2,32.72-28.4.4-.8.8-.8,1.52-.56,3.28,1.12,6.56,3,6.4,4.08-.16.64-1,1-2.56,1a54.64,54.64,0,0,0,32,17.44c2.32.48,2.4,1.6.32,2.48a6.42,6.42,0,0,0-2.72,2.16c-1.2,1.28-2.08,1.52-4.32.88-10.4-3.52-21.2-12-27.2-20.88a64.49,64.49,0,0,1-16,15.52,15.72,15.72,0,0,1,4.48,2.32h22.56c1.12-1.44,2.48-2.88,3.28-2.88,1.68,0,6.48,3.52,6.48,5.28,0,1-1.36,1.76-2.8,2.4-.16,5.2.08,14.48.08,17.12,0,2.48-1.28,3-3.6,3a2.44,2.44,0,0,1-2.48-2.72V67H570ZM558.05,12.06a13.68,13.68,0,0,1,0,2.56c-.56,5.76-4,9.12-8,9.28a2.9,2.9,0,0,1-3.12-3c-.16-3,3.92-4.08,5.92-7.68,1.28-1.92,1.12-4.48,1.2-6.88a1.46,1.46,0,0,1,2.88-.48,17.77,17.77,0,0,1,.72,3h21.52a66.34,66.34,0,0,0-.16-7A1,1,0,0,1,580.13.7c1.36,0,8.8.24,8.8,2.32,0,1.6-1.6,2.24-4.08,2.8v3h23.2c1.28-1.36,2.8-2.72,3.6-2.72,1.44,0,6.72,4.56,6.64,6.88,0,1.6-2.08,1.76-4.24,2.24-2.24,1.84-4.64,3.76-6.48,5-1.68,1.12-3.2,0-2.24-1.84a50.63,50.63,0,0,0,2.88-6.4Zm11.2,5.68c2.64,1.28,6.56,4.32,6.16,5.68-.32,1.12-1.52,1.36-4.16.8-3.2,3.28-7.44,7.36-13,9.84-2.08.8-3.28-.72-1.76-2.24A50.89,50.89,0,0,0,567.33,18.3C567.65,17.66,568.21,17.26,569.25,17.74Zm26.4,46.08c0-4.56.08-10.64.08-13.52H570.05c-.08,2.08-.08,7.6-.08,9.12v4.4Zm9.84-37.28c.48,2.24-.48,4-2.56,4.48-2.24.56-3.68-1.52-5.92-4.32a43,43,0,0,0-5.12-5.6c-1.28-1.28-.8-3,1.36-2.64C598.45,19.34,604.53,22.46,605.49,26.54Z"></path>
                    <path d="M656.72,12.94a20.91,20.91,0,0,1,0,2.24c-.48,5.84-3.84,9.84-8.16,10a3.18,3.18,0,0,1-3.28-3.2c-.16-3.36,4-4.24,6.08-8.24,1.28-2,1.12-4.64,1.2-7a1.47,1.47,0,0,1,2.88-.48c.32,1,.64,2.24.88,3.44H678a62.52,62.52,0,0,0-.24-7.92,1,1,0,0,1,1.12-1.2c1.36,0,9,.24,9,2.4,0,1.6-1.68,2.24-4.08,2.8V9.74h22.8c1.28-1.52,3-3.12,3.92-3.12,1.52,0,6.88,5,6.88,7.12,0,1.76-2.32,1.68-4.32,1.84-1.92,1.6-4.08,3.36-5.52,4.48-1.68,1.12-3,.32-2.16-1.68.32-1.28,1-3.44,1.6-5.44ZM705.12,67c1.68-2.48,3.84-5.36,5-5.36,1.6,0,7.44,4.8,7.44,6.72,0,1.12-1.28,1.84-4.4,1.84H647.68c-1,0-1.76-.32-1.76-1.6s.72-1.6,1.76-1.6H678c0-3.28,0-7.2-.08-11H655.6c-1,0-1.76-.32-1.76-1.6s.72-1.6,1.76-1.6h22.32c-.08-4.08-.24-7.6-.48-9.68-.08-.72.4-1.12,1.12-1.12,1.36,0,9.12.24,9.12,2.32,0,1.68-1.76,2.32-4.24,2.88v5.6h12.08c1.68-2.4,3.84-5.28,5-5.28,1.6,0,7.36,4.72,7.36,6.64,0,1.12-1.36,1.84-4.4,1.84H683.44V67Zm-21.84-41.2a1.06,1.06,0,0,1,.24.64c0,1.12-1.92,1.44-3.68,1.6-1.44,2.32-3.28,5.44-5.28,8.16,9.2-.24,16.88-.72,23.6-1a31,31,0,0,0-3.2-3.92c-1-1-.16-2.56,1.6-2.08,7.52,2.08,11,6.48,11.6,9.68.32,2.24-.88,3.84-2.88,4.08-1.52.24-2.48-.32-3.44-1.68-.48-.8-1-1.84-1.68-3-11.44,2.16-25.92,3-40.8,3.84-.4,1.76-1.12,3.12-2.32,3-1.6-.24-3-3.52-3.28-7.68,0-.72.4-1,1-1,5.44,0,10.4,0,15-.08A47.2,47.2,0,0,0,674,25.82H659c-1,0-1.76-.32-1.76-1.6s.72-1.6,1.76-1.6h35.28c1.68-2.4,3.76-5.2,5-5.2,1.52,0,7.2,4.72,7.2,6.64,0,1.12-1.36,1.76-4.32,1.76Z"></path>
                </g>
            </g>
        </svg><a class="section__top-link" id="topArrow" href="#__concept">
            <div class="section__top-next"><img class="section__top-arrow nextArrow" src="<?php echo get_template_directory_uri(); ?>/images/img_scroll_arrow.png"></div></a>
    </div>
    <div class="section section__concept">
        <div class="c-grid">
            <div class="c-grid__col">
                <div class="section__left slideInLeftWrap">
                    <div class="slidein slideInLeft">
                        <div class="slideInLeftInner">
                            <h2 class="section__title">Concept</h2>
                            <p class="section__title-ja">コンセプト</p><a class="section__title-button u-disp--tablet_inline-block c-button c-button--white" id="openModalConcept" href="javascript:void(0);">More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-grid__col u-disp--pc">
                <div class="section__right slideInRightWrap">
                    <div class="section__contents slidein slideInRight">
                        <div class="section__contents-inner slideInRightInner">
                            <h3 class="section__contents-title">お客様が主役の美容室</h3>
                            <p class="section__contents-text">名前のない美容室へようこそ。<br>所沢駅近くに位置する当美容室はお客様に「サロンの看板ではなくスタイリスト自身に会いにきていただきたい」という想いを持って立ち上げた安らぎの空間です。<br>日々のストレスを少しでも和らげられるようアンティーク家具で店内をまとめ「美容室らしくない」非日常空間を作りました。<br>丁寧なヒアリングをもとにお客様の「本音」をお伺いし、最適なスタイルを提案いたします。<br>どうぞ名前のない美容室をお楽しみください。</p>
                            <!-- <div class="c-button__center"><a class="c-button c-button--black c-button--window" href="<?php /*echo get_template_directory_uri();*/ ?>/images/junmenu.pdf" target="_blank">MENU</a></div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section__gallery">
        <div class="slideInLeftWrap gallery__title">
            <div class="slidein slideInLeft">
                <div class="slideInLeftInner">
                    <h2 class="section__title">Gallery</h2>
                    <p class="section__title-ja">ギャラリー</p><a class="section__title-button c-button c-button--white" href="/gallery/">More</a>
                </div>
            </div>
        </div>
        <div class="slider-container u-disp--pc">
            <div class="scroll_arrow slideInArrow" id="sliderNextGallery"><img class="scroll_arrow-right arrowRightGallery" src="<?php echo get_template_directory_uri(); ?>/images/img_scroll_arrow_right.png"></div>
            <div id="sliderGallery">
                <ul class="list-gallery swiper-wrapper">
                    <div class="swiper-slide list-gallery__item">&nbsp;</div>
                    <div class="swiper-slide list-gallery__item"><img src="<?php echo get_template_directory_uri(); ?>/images/img_gallery01.jpg"></div>
                    <div class="swiper-slide list-gallery__item"><img src="<?php echo get_template_directory_uri(); ?>/images/img_gallery02.jpg"></div>
                    <div class="swiper-slide list-gallery__item"><img src="<?php echo get_template_directory_uri(); ?>/images/img_gallery03.jpg"></div>
                    <div class="swiper-slide list-gallery__item"><img src="<?php echo get_template_directory_uri(); ?>/images/img_gallery04.jpg"></div>
                    <div class="swiper-slide list-gallery__item"><img src="<?php echo get_template_directory_uri(); ?>/images/img_gallery05.jpg"></div>
                </ul>
            </div>
        </div>
    </div>
    <div class="section section__style" data-display="false">
        <div class="slideInLeftWrap style__title">
            <div class="slidein slideInLeft">
                <div class="slideInLeftInner">
                    <h2 class="section__title">Style</h2>
                    <p class="section__title-ja">スタイル</p><a class="section__title-button c-button c-button--white" href="/style">More</a>
                </div>
            </div>
        </div>
        <div class="slider-container u-disp--pc">
            <div class="scroll_arrow slideInArrow" id="sliderNextStyle"><img class="scroll_arrow-right arrowRightStyle" src="<?php echo get_template_directory_uri(); ?>/images/img_scroll_arrow_right.png"></div>
            <div id="sliderStyle">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">&nbsp;</div>
                    <div class="swiper-slide">&nbsp;</div>
                    <?php
                        $arg = array(
                            'posts_per_page' => 4, // 表示する件数
                            'orderby' => 'date', // 日付でソート
                            'order' => 'DESC', // DESCで最新から表示、ASCで最古から表示
                            'category_name' => 'style' // 表示したいカテゴリーのスラッグを指定
                        );
                        $posts = get_posts( $arg );
                        $post_num =  count($posts);
                    ?>
                    <?php if( $posts ): ?>
                        <?php foreach ( $posts as $post ) : ?>
                        <?php setup_postdata( $post ); ?>
                            <?php $imageId = get_post_thumbnail_id();?>
                            <?php $imageUrl = wp_get_attachment_image_src($imageId, true); ?>
                            <?php $thumUrl = (is_array($imageUrl) && $imageUrl[0] !== '') ? $imageUrl[0] : get_template_directory_uri() . '/images/img_dummy.png'; ?>
                            <div class="swiper-slide">
                                <article class="list-article">
                                    <a class="list-article__link" href="<?php the_permalink(); ?>">
                                        <div class="list-article__thumb">
                                            <img src="<?php echo $thumUrl; ?>" class="list-article__image">
                                        </div>
                                        <div class="list-article__contents">
                                            <header>
                                                <div class="list-article__date"><?php the_time( 'Y.m.d' ); ?></div>
                                                <h3 class="list-article__title"><?php the_title(); ?></h3>
                                            </header>
                                            <div class="list-article__text"><?php the_excerpt(); ?></div>
                                        </div>
                                    </a>
                                </article>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section section__news">
        <div class="c-grid">
            <div class="c-grid__col">
                <div class="section__left slideInLeftWrap">
                    <div class="slidein slideInLeft">
                        <div class="slideInLeftInner">
                            <h2 class="section__title">News</h2>
                            <p class="section__title-ja">ニュース</p><a class="section__title-button u-disp--tablet_inline-block c-button c-button--white" href="/news/">More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-grid__col u-disp--pc">
                <div class="section__right slideInRightWrap">
                    <div class="section__contents slidein slideInRight">
                        <div class="section__contents-inner slideInRightInner">
                            <?php
                            $arg = array(
                                'posts_per_page' => 3, // 表示する件数
                                'orderby' => 'date', // 日付でソート
                                'order' => 'DESC', // DESCで最新から表示、ASCで最古から表示
                                'category_name' => 'news' // 表示したいカテゴリーのスラッグを指定
                            );
                            $posts = get_posts( $arg );
                            $post_num =  count($posts);
                            ?>
                            <ul class="list-news">
                                <?php if( $posts ): ?>
                                    <?php foreach ( $posts as $post ) : ?>
                                        <?php setup_postdata( $post ); ?>
                                            <li class="list-news__item">
                                                <a class="list-news__link" href="<?php the_permalink(); ?>">
                                                    <div class="list-news__date"><?php the_time( 'Y.m.d' ); ?></div>
                                                    <div class="list-news__item__contents">
                                                        <h3 class="list-news__title"><?php the_title(); ?></h3>
                                                        <p class="list-news__text"><?php the_excerpt(); ?></p>
                                                    </div>
                                                </a>
                                            </li>
                                    <?php endforeach; ?>
                                <?php endif; wp_reset_postdata(); ?>
                                <?php if ($post_num < 3) : ?>
                                    <li class="list-news__item">
                                        <a class="list-news__link" href="javascript:void(0);">
                                            <div class="list-news__date">2019.02.28</div>
                                            <div class="list-news__item__contents">
                                                <h3 class="list-news__title">ホームページを公開いたしました。</h3>
                                            </div>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                            <div class="c-button__center"><a class="c-button c-button--black" href="/news">More</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section__access">
        <div class="c-grid">
            <div class="c-grid__col">
                <div class="section__left slideInLeftWrap">
                    <div class="slidein slideInLeft">
                        <div class="slideInLeftInner">
                            <h2 class="section__title">Access</h2>
                            <p class="section__title-ja">アクセス</p><a class="section__title-button u-disp--tablet_inline-block c-button c-button--white" id="openModalAccess" href="javascript:void(0);">More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-grid__col u-disp--pc">
                <div class="section__right slideInRightWrap">
                    <div class="section__contents slidein slideInRight">
                        <div class="scroll_arrow slideInArrow"><img src="<?php echo get_template_directory_uri(); ?>/images/img_scroll_arrow_black.png"></div>
                        <div class="section__contents-inner slideInRightInner">
                            <dl class="list-access">
                                <dt class="list-access__title">電話番号</dt>
                                <dd class="list-access__desc">04-2941-6576</dd>
                                <dt class="list-access__title">住所</dt>
                                <dd class="list-access__desc">埼玉県所沢市北秋津776-10</dd>
                                <dt class="list-access__title">営業時間</dt>
                                <dd class="list-access__desc">月曜日～日曜日<br>10:00～23:00<br>カット最終受付22:00</dd>
                                <dt class="list-access__title">定休日</dt>
                                <dd class="list-access__desc">不定休</dd>
                                <dt class="list-access__title">対応決済</dt>
                                <dd class="list-access__desc">各種クレジットカード、QUICPay、Suica</dd>
                                <dt class="list-access__title">駐車場</dt>
                                <dd class="list-access__desc">1台有り</dd>
                                <dt class="list-access__title">道順</dt>
                                <dd class="list-access__desc">所沢駅東口を出て、目の前のバス通りをまっすぐ進みます。「所沢中央病院」を通り過ぎ、右手の「ファミリーマート」を右折し1分程歩いていただくと、左手に当サロン【名前のない美容室】がございます。</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
