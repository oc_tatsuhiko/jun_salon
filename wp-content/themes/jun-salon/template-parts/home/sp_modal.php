<?php
/**
 * The template for displaying the home modal
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */

?>
<div class="concept-modal sp-modal" id="conceptModal">
    <div class="sp-modal__close"><a class="sp-modal__close-link spModalClose" href="javascript:void(0);"><span></span><span></span><span></span></a></div>
    <div class="sp-modal__wrap">
        <h2 class="sp-modal__title">Concept</h2>
        <p class="sp-modal__title-ja">コンセプト</p>
        <div class="sp-modal__contents">
            <div class="sp-modal__contents-title">お客様が主役の美容室</div>
            <div class="sp-modal__contents-text">名前のない美容室へようこそ。<br>所沢駅近くに位置する当美容室はお客様に「サロンの看板ではなくスタイリスト自身に会いにきていただきたい」という想いを持って立ち上げた安らぎの空間です。<br>日々のストレスを少しでも和らげられるようアンティーク家具で店内をまとめ「美容室らしくない」非日常空間を作りました。<br>丁寧なヒアリングをもとにお客様の「本音」をお伺いし、最適なスタイルを提案いたします。<br>どうぞ名前のない美容室をお楽しみください。</div>
            <!-- <div class="c-button__center"><a class="c-button c-button--black c-button--window" href="<?php /*echo get_template_directory_uri();*/ ?>/images/junmenu.pdf" target="_blank">MENU</a></div> -->
        </div>
    </div>
</div>
<div class="access-modal sp-modal" id="accessModal">
    <div class="sp-modal__close"><a class="sp-modal__close-link spModalClose" href="javascript:void(0);"><span></span><span></span><span></span></a></div>
    <div class="sp-modal__wrap">
        <h2 class="sp-modal__title">Access</h2>
        <p class="sp-modal__title-ja">アクセス</p>
        <div class="sp-modal__contents">
            <dl class="sp-modal__list">
                <dt class="sp-modal__list-title">電話番号</dt>
                <dd class="sp-modal__list-desc">04-2941-6576</dd>
                <dt class="sp-modal__list-title">住所</dt>
                <dd class="sp-modal__list-desc">埼玉県所沢市北秋津776-10</dd>
                <dt class="sp-modal__list-title">営業時間</dt>
                <dd class="sp-modal__list-desc">月曜日～日曜日<br>10:00～23:00<br>カット最終受付22:00</dd>
                <dt class="sp-modal__list-title">定休日</dt>
                <dd class="sp-modal__list-desc">不定休</dd>
                <dt class="sp-modal__list-title">対応決済</dt>
                <dd class="sp-modal__list-desc">各種クレジットカード、QUICPay、Suica</dd>
                <dt class="sp-modal__list-title">駐車場</dt>
                <dd class="sp-modal__list-desc">1台有り</dd>
                <dt class="sp-modal__list-title">道順</dt>
                <dd class="sp-modal__list-desc">所沢駅東口を出て、目の前のバス通りをまっすぐ進みます。「所沢中央病院」を通り過ぎ、右手の「ファミリーマート」を右折し１分程歩いていただくと、左手に当サロン【名前のない美容室】がございます。</dd>
            </dl>
        </div>
    </div>
</div>
