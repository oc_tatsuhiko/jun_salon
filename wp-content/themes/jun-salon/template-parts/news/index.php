<?php
$paged = (int) get_query_var('paged');
$arg = array(
    'posts_per_page' => 9, // 表示する件数
    'paged' => $paged,
    'orderby' => 'date', // 日付でソート
    'order' => 'DESC', // DESCで最新から表示、ASCで最古から表示
    'category_name' => 'news', // 表示したいカテゴリーのスラッグを指定
    'post_status' => 'publish'
);
$posts = get_posts( $arg );
$post_num =  count($posts);
?>
<main class="main">
    <div class="content-header content-header--news">
        <div class="content-header__title">
            <h2 class="content-header__title--en">News</h2>
            <p class="content-header__title--ja">ニュース</p>
        </div>
    </div>
    <div class="container">
        <?php breadcrumb(); ?>
        <div class="container__inner">
            <?php if( $posts ): ?>
                <ul class="list-page-news">
                    <?php foreach ( $posts as $post ) : ?>
                        <?php setup_postdata( $post ); ?>
                        <li class="list-page-news__item"><a class="list-page-news__link" href="<?php the_permalink(); ?>">
                            <div class="list-page-news__date"><?php the_time( 'Y.m.d' ); ?></div>
                            <div class="list-page-news__item__contents">
                                    <h3 class="list-page-news__title"><?php the_title(); ?>
                                    </h3>
                                    <p class="list-page-news__text"><?php the_excerpt(); ?></p>
                                </div></a></li>
                        </li>
                    <?php endforeach; ?>
                    <?php if ($post_num < 5) : ?>
                        <li class="list-page-news__item"><a class="list-page-news__link" href="javascripr:void(0);">
                                <div class="list-page-news__date">2019.02.28</div>
                                <div class="list-page-news__item__contents">
                                    <h3 class="list-page-news__title">ホームページを公開いたしました。
                                    </h3>
                                    <p class="list-page-news__text"></p>
                                </div></a></li>
                    <?php endif; ?>
                </ul>
            <?php endif; wp_reset_postdata();?>
            <?php pagination($wp_query->max_num_pages, get_query_var( 'paged' )); ?>
        </div>
    </div>
</main>
