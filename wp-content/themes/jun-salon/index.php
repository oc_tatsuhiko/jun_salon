<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage jun-salon
 * @since jun-salon 1.0
 */
?>
<?php get_template_part('template-parts/common/head'); ?>
<?php get_header(); ?>
<?php get_template_part('template-parts/home/movie'); ?>
<?php get_template_part('template-parts/home/contents'); ?>
<?php get_template_part('template-parts/home/loading'); ?>
<?php get_template_part('template-parts/home/sp_modal'); ?>
<?php get_template_part('template-parts/common/sp_nav'); ?>
<?php get_template_part('template-parts/common/sns'); ?>
<?php get_footer(); ?>
<?php get_template_part('template-parts/common/script'); ?>
